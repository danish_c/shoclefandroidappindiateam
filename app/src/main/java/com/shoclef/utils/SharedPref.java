package com.shoclef.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPref {

    private static SharedPreferences shr;
    private static SharedPreferences.Editor editor;
    private static String prefName = "com.shoclef.utils.SharedPref";


    // SharedPreferences Keys
    public static String autoLogin = "autoLogin";//Boolean
    public static String fragmentName = "fragmentName";//String
    public static String userId = "userId";//String
    public static String accessToken = "accessToken";//String
    public static String userName = "userName";//String
    public static String userNumber = "userNumber";//String
    public static String userCountryCode = "userCountryCode";//String
    public static String userEmailId = "userEmailId";//String
    public static String userCity = "userCity";//String
    public static String userState = "userState";//String
    public static String userCcountry = "userCcountry";//String
    public static String tncActive = "tncActive";//Boolean
    public static String categoryInitialList = "categoryInitialList";//Boolean


    public static void saveString(String className, Context context, String key, String value) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        editor = shr.edit();
        editor.putString(key, value).apply();
        LoggerClass.i(className + "SharedPref saveString", "Key:" + key + " value:" + value);
    }


    public static void saveInt(String className, Context context, String key, int value) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        editor = shr.edit();
        editor.putInt(key, value).apply();
        LoggerClass.i(className + "SharedPref saveInt", "Key:" + key + " value:" + value);
    }

    public static void saveBoolean(String className, Context context, String key, boolean value) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        editor = shr.edit();
        editor.putBoolean(key, value).apply();
        LoggerClass.i(className + "SharedPref saveBoolean", "Key:" + key + " value:" + value);
    }

    public static void saveLong(String className, Context context, String key, long value) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        editor = shr.edit();
        editor.putLong(key, value).apply();
        LoggerClass.i(className + "SharedPref saveLong", "Key:" + key + " value:" + value);
    }

    public static String getString(String className, Context context, String key) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        String data = shr.getString(key, "nothing is saved here");
        LoggerClass.i(className + "SharedPref getString", "Key:" + key + " value:" + data);
        return data;
    }

    public static boolean getBoolean(String className, Context context, String key) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        boolean data = shr.getBoolean(key, false);
        LoggerClass.i(className + "SharedPref getBoolean", "Key:" + key + " value:" + data);
        return data;
    }

    public static int getInt(String className, Context context, String key) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        int data = shr.getInt(key, 0);
        LoggerClass.i(className + "SharedPref getInt", "Key:" + key + " value:" + data);
        return data;
    }

    public static long getLong(String className, Context context, String key) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        long data = shr.getLong(key, 0);
        LoggerClass.i(className + "SharedPref getLong", "Key:" + key + " value:" + data);
        return data;
    }

    public static void removeshr(String className, Context context, String key) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        try {
            editor = shr.edit();
            editor.remove(key).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LoggerClass.i(className + "SharedPref removeshr", "Key:" + key);
    }


    public static void cleardata(String className, Context context) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        editor = shr.edit();
        editor.clear().apply();
        LoggerClass.i(className + "SharedPref ", "SharedPref Cleared");
    }

    public static String getall(String className, Context context) {
        shr = context.getSharedPreferences(prefName, MODE_PRIVATE);
        String allShared = shr.getAll().toString().replace(",", ",\n");
        LoggerClass.i(className + "GET ALL SHARED======================", shr.getAll().toString().replace(",", ",\n"));
//        Toast.makeText(context, shr.getAll().toString(), Toast.LENGTH_LONG).show();
        return allShared;
    }
}
