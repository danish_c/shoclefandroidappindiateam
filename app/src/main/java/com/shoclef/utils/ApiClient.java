package com.shoclef.utils;

public class ApiClient {
    public static String BASE_URL = "http://ec2-54-183-245-83.us-west-1.compute.amazonaws.com:";
    public static String userLogin = BASE_URL + "3011/secureuser/signin";
    public static String userGenerateOtp = BASE_URL + "3011/secureuser/changepassword";
    public static String userVerifyOtp = BASE_URL + "3011/secureuser/updatepassword";
    public static String userRegistrationVerification = BASE_URL + "3011/registration/verification";
    public static String userUpdatePassword = BASE_URL + "3011/secureuser/updateuserpassword";
    public static String userRegistration = BASE_URL + "3011/registration/register";
    public static String goodsCategoryList = BASE_URL + "3011/categorydetails/getCategoryMapping?CategoryId=";
    public static String productDetail = BASE_URL + "3011/productdetails/getproductdata?ProductId=";
    public static String deleteFromCartApi = BASE_URL + "3019/addtocart/deleteCartDetails?";
    public static String updateCartQty = BASE_URL + "3019/addtocart/updateCartDetails?";
    public static String getCartDetails = BASE_URL + "3019/addtocart/getCartDetails?user_id=";
    public static String addToCartApi = BASE_URL + "3019/addtocart/addDetails";
    public static String getShippingAmount = BASE_URL + "3019/shippingdetails/shipment";
    public static String getUserApi = BASE_URL + "3019/shipdetails/getShipAddressDetailsById?user_id=";
    public static String getDataFromPincodeUlr = "https://api.postalpincode.in/pincode/";
    public static String addAddressUrl = BASE_URL + "3019/shipdetails/addAddressDetails";
    public static String getCountryUrl = BASE_URL + "3019/shipdetails/getCountry";
    public static String getStateUrl = BASE_URL + "3019/shipdetails/getStates?country_id=";
    public static String getCityUrl = BASE_URL + "3019/shipdetails/getCities?state_id=";
    public static String paypal = BASE_URL + "3011/secureuser/paypalcheckout";
    public static String getInvoiceList = BASE_URL + "3019/customer/invoicedetails?user_id=";
    public static String localServiceDetail = BASE_URL + "3011/servicedetails/getLocalServiceDetails";
    public static String onlineServiceDetail = BASE_URL + "3011/servicedetails/getOnlineServiceDetails";


    public static String productGoodsList(String pageNumber, String limit, String categoryId) {
        String url;
        if (categoryId.equals("")) {
            url = BASE_URL + "3011/productdetails/getproductlist?page=" + pageNumber + "&limit=" + limit;
        } else {
            url = BASE_URL + "3011/productdetails/getproductlist?page=" + pageNumber + "&limit=" + limit + "&CategoryId=" + categoryId;

        }
        return url;
    }

}
