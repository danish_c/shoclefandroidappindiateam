package com.shoclef.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.shoclef.R;
import com.shoclef.activities.SplashScreenActivity;
import com.shoclef.activities.FragmentActivity;
import com.shoclef.utils.shakeAnim.Techniques;
import com.shoclef.utils.shakeAnim.YoYo;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class util {
    public static void getFullScreen(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void showToast(String text, Context context) {
        try {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void gotoNextActivity(Activity currentActivity, Class<?> aClass) {
        Intent intent = new Intent(currentActivity.getApplicationContext(), aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        currentActivity.getApplicationContext().startActivity(intent);
        currentActivity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    public static void gotoFragmentActivity(Activity currentActivity, String mode) {
        Intent intent = new Intent(currentActivity.getApplicationContext(), FragmentActivity.class);
        intent.putExtra("fragmentName", mode);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        currentActivity.getApplicationContext().startActivity(intent);
        currentActivity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }


    public static void gotoPrevActivity(Activity currentActivity, Class<?> aClass) {
        Intent intent = new Intent(currentActivity.getApplicationContext(), aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        currentActivity.getApplicationContext().startActivity(intent);
        currentActivity.overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

    }

    public static void gotoNextFragment(@IdRes int containerViewId, Fragment nextFragment, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(containerViewId, nextFragment)
                .addToBackStack(null)
                .commit();
    }

    public static void gotoPrevFragment(@IdRes int containerViewId, Fragment prevFragment, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.replace(containerViewId, prevFragment)
                .addToBackStack(null)
                .commit();
    }

    public static void openFragment(@IdRes int containerViewId, Fragment fragment, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment)
                .addToBackStack(String.valueOf(fragment))
                .commit();
    }


    public static void closekeybaord(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    public static void showKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInputFromWindow(
                    view.getApplicationWindowToken(),
                    InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static void setError(TextInputLayout textInputLayout, TextInputEditText textInputEditText, String errorString) {
        textInputEditText.requestFocus();
        YoYo.with(Techniques.Shake).playOn(textInputLayout);
        textInputLayout.setError(errorString);


    }

    public static boolean validatePassword(String password) {
        final String passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=\\S+$).{6,}$";
        Pattern pattern = Pattern.compile(passwordPattern);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void removeError(TextInputLayout textInputLayout) {
        textInputLayout.setErrorEnabled(false);
    }

    public static void logout(Context context, Activity currentActivity) {
        SharedPref.cleardata("Logout", context);
        util.gotoNextActivity(currentActivity, SplashScreenActivity.class);
        currentActivity.finish();
    }

    public static String getSerial() {
        return Build.SERIAL;
    }

    public static String Base64Encoder(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);//compressing the bitmap
        byte[] byteArray = byteArrayOutputStream.toByteArray();//converting the outputStream to a byteArray
        return Base64.encodeToString(byteArray, Base64.DEFAULT);//return the encoded value
    }

}
