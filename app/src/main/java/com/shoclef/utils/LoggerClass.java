package com.shoclef.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.shoclef.activities.ErrorScreenActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class LoggerClass {

    private static boolean logger = true;

    public static void LogApi(String className, String methodName, String apiUrl, String apiParams) {
        Log.i(className + " Api " + "Method ", methodName);
        Log.i(className + " Api " + "URL ", apiUrl);
        Log.i(className + " Api " + "Params", apiParams.replace("{", "\n{\n").trim().replace(",", ",\n").trim().replace("}", "\n}\n").trim());
    }

    public static void LogApiResponse(String className, String methodName, JSONObject apiResponse) {
        try {
            Log.i(className + ":" + methodName + ":Response: ", apiResponse.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(className, e.toString());
        }
    }


    @SuppressLint("LongLogTag")
    public static void LogApiError(Activity currentActivity, String className, String methodName, String url, VolleyError error) {
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            util.showToast("No Connection or Time out Error", currentActivity.getApplicationContext());
        } else if (error instanceof AuthFailureError) {
            util.showToast("AuthFailureError", currentActivity.getApplicationContext());
        } else if (error instanceof ServerError) {
            util.showToast("ServerError", currentActivity.getApplicationContext());
        } else if (error instanceof NetworkError) {
            util.showToast("NetworkError", currentActivity.getApplicationContext());
        } else if (error instanceof ParseError) {
            util.showToast("ParseError", currentActivity.getApplicationContext());
        } else {
            util.showToast(String.valueOf(error), currentActivity.getApplicationContext());
        }
        util.gotoNextActivity(currentActivity, ErrorScreenActivity.class);
        try {
            Log.e(className + "Api " + "Url " + url + " Method: " + methodName + " Error: ", String.valueOf(error));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(className, e.toString());
        }

    }

    public static void LogJson(String TAG, JSONObject jsonObject) {
        try {
            Log.i(TAG, jsonObject.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());

        }
    }

    public static void bundle(String activityName, Bundle bundle) {
        if (logger) {
            Log.i(activityName + " bundle ", String.valueOf(bundle).replace(",", ",\n"));
        }
    }

    public static void i(String activityName, String content) {
        if (logger) {
            Log.i(activityName, content);
        }
    }

    public static void e(String activityName, String content) {
        if (logger) {
            Log.e(activityName + "Exception ", content);
        }
    }

    public static void d(String activityName, String content) {
        if (logger) {
            Log.d(activityName + "Exception ", content);
        }
    }

    public static void responseerror(String activityName, String content) {
        if (logger) {
            Log.e(activityName + "Response Error ", content);
        }
    }
}
