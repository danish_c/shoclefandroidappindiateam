package com.shoclef.adapters.goods;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.shoclef.R;
import com.shoclef.interfaces.goods.GoodsGalleryInterface;
import com.shoclef.objects.goods.GoodsGalleryObject;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GoodsGalleryAdapter extends RecyclerView.Adapter<GoodsGalleryAdapter.ViewHolder> {
    private Context context;
    private GoodsGalleryObject object;
    private ArrayList<GoodsGalleryObject> arrayList;
    private GoodsGalleryInterface anInterface;

    public GoodsGalleryAdapter(Context context, GoodsGalleryObject object, ArrayList<GoodsGalleryObject> arrayList, GoodsGalleryInterface anInterface) {
        this.context = context;
        this.object = object;
        this.arrayList = arrayList;
        this.anInterface = anInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_goods_gallery, parent, false);
        GoodsGalleryAdapter.ViewHolder viewHolder = new GoodsGalleryAdapter.ViewHolder(view);
        viewHolder.ivGallery = view.findViewById(R.id.iv_gallery);
        viewHolder.pbGallery = view.findViewById(R.id.pb_gallery);
        viewHolder.clParent = view.findViewById(R.id.cl_parent_goods_gallery);
        return viewHolder;    }

    @Override
    public void onBindViewHolder(@NonNull final GoodsGalleryAdapter.ViewHolder holder, int position) {
        object = arrayList.get(position);
        final String imageUrl = object.getImageUrl();
        Picasso.get().load(imageUrl).error(R.drawable.ic_logo).into(holder.ivGallery, new Callback() {
            @Override
            public void onSuccess() {
                holder.pbGallery.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });
        holder.clParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                anInterface.onClick(imageUrl);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivGallery;
        ProgressBar pbGallery;
        ConstraintLayout clParent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}