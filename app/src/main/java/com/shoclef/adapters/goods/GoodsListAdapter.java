package com.shoclef.adapters.goods;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.shoclef.R;
import com.shoclef.interfaces.goods.GoodsListInterface;
import com.shoclef.objects.goods.GoodsListObject;
import com.shoclef.utils.Fonts;
import com.shoclef.utils.util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GoodsListAdapter extends RecyclerView.Adapter<GoodsListAdapter.ViewHolder> {
    private Context context;
    private GoodsListObject object;
    private ArrayList<GoodsListObject> arrayList;
    private GoodsListInterface anInterface;

    public GoodsListAdapter(Context context, GoodsListObject object, ArrayList<GoodsListObject> arrayList, GoodsListInterface anInterface) {
        this.context = context;
        this.object = object;
        this.arrayList = arrayList;
        this.anInterface = anInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_goods_listening, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.cvParent = view.findViewById(R.id.cv_parent_goods);
        viewHolder.ivProduct = view.findViewById(R.id.iv_product_goods);
        viewHolder.tvDiscountPercentage = view.findViewById(R.id.tv_discount_percentage_goods);
        viewHolder.tvProductName = view.findViewById(R.id.tv_product_name_goods);
        viewHolder.tvDiscountedPrice = view.findViewById(R.id.tv_discounted_price_goods);
        viewHolder.tvPrice = view.findViewById(R.id.tv_price_goods);
        viewHolder.tvBought = view.findViewById(R.id.tv_bought_goods);
        viewHolder.tvRating = view.findViewById(R.id.tv_rating_goods);
        viewHolder.pb = view.findViewById(R.id.pb_goods);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        object = arrayList.get(position);
        if (position == arrayList.size() - 1) {
            anInterface.onBottomReached(position);
        }
        final String productId = object.getProductId();
        final String productImageUrl = object.getProductImageUrl();
        final String productRating = object.getProductRating();
        final String productName = object.getProductName();
        final String productDiscountedPercentage = object.getProductDiscountedPercentage();
        final String productDiscountedPrice = object.getProductDiscountedPrice();
        final String productPrice = object.getProductPrice();
        final String productBought = object.getProductBought();
        if (productDiscountedPercentage.equals("NA")) {
            holder.tvDiscountPercentage.setVisibility(View.GONE);
            holder.tvPrice.setVisibility(View.GONE);
        } else {
            holder.tvDiscountPercentage.setText(productDiscountedPercentage + " % off");
            holder.tvPrice.setText(productPrice);
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        holder.tvProductName.setText(productName);

        holder.tvDiscountedPrice.setText(productDiscountedPrice);

        holder.tvBought.setText(productBought + " Bought");
        holder.tvRating.setText(productRating);
        Picasso.get().load(productImageUrl).error(R.drawable.ic_logo).into(holder.ivProduct, new Callback() {
            @Override
            public void onSuccess() {
                holder.pb.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                util.showToast("There's some problem while loading images", context);
            }
        });
        holder.cvParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                anInterface.onClick(productId, productName, arrayList.get(position).getJsonObject());
            }
        });
        setFonts(holder);
    }


    private void setFonts(ViewHolder holder) {
        Typeface light = Fonts.getLight(context);
        Typeface regular = Fonts.getRegular(context);
        Typeface medium = Fonts.getMedium(context);
        holder.tvProductName.setTypeface(medium);
        holder.tvDiscountedPrice.setTypeface(regular);
        holder.tvPrice.setTypeface(light);
        holder.tvBought.setTypeface(light);
        holder.tvRating.setTypeface(medium);
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvParent;
        ImageView ivProduct;
        TextView tvDiscountPercentage, tvProductName, tvRating, tvDiscountedPrice, tvPrice, tvBought;
        ProgressBar pb;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
