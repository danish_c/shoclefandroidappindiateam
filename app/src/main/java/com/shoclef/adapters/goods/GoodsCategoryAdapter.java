package com.shoclef.adapters.goods;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.shoclef.R;
import com.shoclef.interfaces.goods.GoodsCategoryInterface;
import com.shoclef.objects.goods.GoodsCategoryObject;
import com.shoclef.utils.Fonts;

import java.util.ArrayList;

public class GoodsCategoryAdapter extends RecyclerView.Adapter<GoodsCategoryAdapter.ViewHolder> {
    private Context context;
    private GoodsCategoryObject object;
    private ArrayList<GoodsCategoryObject> arrayList;
    private GoodsCategoryInterface anInterface;
    private static int index = -1;

    public GoodsCategoryAdapter(Context context, GoodsCategoryObject object, ArrayList<GoodsCategoryObject> arrayList, GoodsCategoryInterface anInterface) {
        this.context = context;
        this.object = object;
        this.arrayList = arrayList;
        this.anInterface = anInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_category, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.tvCategoryName = view.findViewById(R.id.tv_category_name);
        viewHolder.clCategory = view.findViewById(R.id.cl_category);
        viewHolder.tvCategoryName.setTypeface(Fonts.getMedium(context));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        object = arrayList.get(position);
        final String CategoryId = object.getCategoryId();
        final String CategoryName = object.getCategoryName();
        holder.tvCategoryName.setText(CategoryName);
        holder.tvCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                anInterface.onClick(CategoryId, CategoryName);
                index = position;
                notifyDataSetChanged();
            }
        });
        holder.clCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                anInterface.onClick(CategoryId, CategoryName);
                index = position;
                notifyDataSetChanged();
            }
        });
        if (index == position) {
            holder.tvCategoryName.setTextColor(Color.parseColor("#00838f"));
        } else {
            holder.tvCategoryName.setTextColor(Color.parseColor("#000000"));
        }
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategoryName;
        ConstraintLayout clCategory;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
