package com.shoclef.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shoclef.R;
import com.shoclef.interfaces.InvoiceInterface;
import com.shoclef.objects.InvoiceObject;
import com.shoclef.utils.util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.ViewHolder> {
    private Context context;
    private InvoiceObject object;
    private ArrayList<InvoiceObject> arrayList;
    private InvoiceInterface anInterface;

    public InvoiceAdapter(Context context, InvoiceObject object, ArrayList<InvoiceObject> arrayList, InvoiceInterface anInterface) {
        this.context = context;
        this.object = object;
        this.arrayList = arrayList;
        this.anInterface = anInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_invoice, parent, false);
        InvoiceAdapter.ViewHolder viewHolder = new InvoiceAdapter.ViewHolder(view);
        viewHolder.ivProduct = view.findViewById(R.id.iv_product_invoice);
        viewHolder.imgCircleStatus1 = view.findViewById(R.id.img_circle_status_1_invoice);
        viewHolder.imgCircleStatus2 = view.findViewById(R.id.img_circle_status_2_invoice);
        viewHolder.imgCircleStatus3 = view.findViewById(R.id.img_circle_status_3_invoice);
        viewHolder.imgCircleStatus4 = view.findViewById(R.id.img_circle_status_4_invoice);
        viewHolder.imgLine1 = view.findViewById(R.id.img_line_1_invoice);
        viewHolder.imgLine2 = view.findViewById(R.id.img_line_2_invoice);
        viewHolder.imgLine3 = view.findViewById(R.id.img_line_3_invoice);
        viewHolder.tvName = view.findViewById(R.id.tv_product_name_invoice);
        viewHolder.tvAttributes = view.findViewById(R.id.tv_attributes_invoice);
        viewHolder.tvQuantity = view.findViewById(R.id.tv_quantity_invoice);
        viewHolder.tvPrice = view.findViewById(R.id.tv_price_invoice);
        viewHolder.tvPurchaseDate = view.findViewById(R.id.tv_purchase_date_invoice);
        viewHolder.tvPurchaseId = view.findViewById(R.id.tv_purchase_id_invoice);
        viewHolder.tvTrackingId = view.findViewById(R.id.tv_tracking_id_invoice);
        viewHolder.tvStatus = view.findViewById(R.id.tv_status_invoice);
        viewHolder.tvScheduledDeliveryDate = view.findViewById(R.id.tv_scheduled_delivery_invoice);
        viewHolder.tvStatus1 = view.findViewById(R.id.tv_status_1_invoice);
        viewHolder.tvStatus2 = view.findViewById(R.id.tv_status_2_invoice);
        viewHolder.tvStatus3 = view.findViewById(R.id.tv_status_3_invoice);
        viewHolder.tvStatus4 = view.findViewById(R.id.tv_status_4_invoice);
        viewHolder.btnCancel = view.findViewById(R.id.btn_cancel_invoice);
        viewHolder.pbMain = view.findViewById(R.id.pb_main_invoice);
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        object = arrayList.get(position);
        String imageUrl = object.getImageUrl();
        String productName = object.getProductName();
        String productAttributes = object.getProductAttributes();
        String purchaseId = object.getProductInvoiceId();
        String productQuantity = object.getProductQuantity();
        String productPrice = object.getProductPrice();
        String productCurrency = object.getProductCurrency();
        String productPurchaseDate = object.getProductPurchaseDate();
        String productTrackingId = object.getProductTrackingId();
        String productStatus = object.getProductStatus();
        String productScheduledDelivery = object.getProductScheduledDelivery();
        String productShippingStatus = object.getProductShippingStatus();
        Picasso.get().load(imageUrl).error(R.drawable.ic_logo).into(holder.ivProduct, new Callback() {
            @Override
            public void onSuccess() {
                holder.pbMain.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                util.showToast(String.valueOf(e), context);
            }
        });

        holder.tvName.setText("Product Name: " + productName);
        holder.tvAttributes.setText("Selected: " + productAttributes);
        holder.tvQuantity.setText("Quantity: " + productQuantity);
        holder.tvPrice.setText(productPrice + " " + productCurrency);
        holder.tvPurchaseDate.setText("Purchase Date: " + productPurchaseDate);
        holder.tvTrackingId.setText("Tracking # " + productTrackingId);
        holder.tvStatus.setText("Status: " + productStatus);
        holder.tvScheduledDeliveryDate.setText("Scheduled Delivery: " + productScheduledDelivery);
        holder.tvPurchaseId.setText("Purchase ID: " + purchaseId);
        setShippingTrackingStatus(productShippingStatus, holder);

    }

    private void setShippingTrackingStatus(String status, ViewHolder holder) {
        switch (status) {
            case "1":
                holder.imgCircleStatus1.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine1.setImageResource(R.drawable.ic_state_not_completed_line);
                holder.imgCircleStatus2.setImageResource(R.drawable.ic_state_not_completed_oval);
                holder.imgLine2.setImageResource(R.drawable.ic_state_not_completed_line);
                holder.imgCircleStatus3.setImageResource(R.drawable.ic_state_not_completed_oval);
                holder.imgLine3.setImageResource(R.drawable.ic_state_not_completed_line);
                holder.imgCircleStatus4.setImageResource(R.drawable.ic_state_not_completed_oval);
                holder.tvStatus1.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus2.setTextColor(Color.parseColor("#99000000"));
                holder.tvStatus3.setTextColor(Color.parseColor("#99000000"));
                holder.tvStatus4.setTextColor(Color.parseColor("#99000000"));
                break;
            case "2":
                holder.imgCircleStatus1.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine1.setImageResource(R.drawable.ic_state_completed_line);
                holder.imgCircleStatus2.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine2.setImageResource(R.drawable.ic_state_not_completed_line);
                holder.imgCircleStatus3.setImageResource(R.drawable.ic_state_not_completed_oval);
                holder.imgLine3.setImageResource(R.drawable.ic_state_not_completed_line);
                holder.imgCircleStatus4.setImageResource(R.drawable.ic_state_not_completed_oval);
                holder.tvStatus1.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus2.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus3.setTextColor(Color.parseColor("#99000000"));
                holder.tvStatus4.setTextColor(Color.parseColor("#99000000"));
                break;
            case "3":
                holder.imgCircleStatus1.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine1.setImageResource(R.drawable.ic_state_completed_line);
                holder.imgCircleStatus2.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine2.setImageResource(R.drawable.ic_state_completed_line);
                holder.imgCircleStatus3.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine3.setImageResource(R.drawable.ic_state_not_completed_line);
                holder.imgCircleStatus4.setImageResource(R.drawable.ic_state_not_completed_oval);
                holder.tvStatus1.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus2.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus3.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus4.setTextColor(Color.parseColor("#99000000"));
                break;
            case "4":
                holder.imgCircleStatus1.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine1.setImageResource(R.drawable.ic_state_completed_line);
                holder.imgCircleStatus2.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine2.setImageResource(R.drawable.ic_state_completed_line);
                holder.imgCircleStatus3.setImageResource(R.drawable.ic_state_completed_oval);
                holder.imgLine3.setImageResource(R.drawable.ic_state_completed_line);
                holder.imgCircleStatus4.setImageResource(R.drawable.ic_state_completed_oval);
                holder.tvStatus1.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus2.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus3.setTextColor(Color.parseColor("#289da1"));
                holder.tvStatus4.setTextColor(Color.parseColor("#289da1"));
                break;
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView
                ivProduct,
                imgCircleStatus1,
                imgCircleStatus2,
                imgCircleStatus3,
                imgCircleStatus4,
                imgLine1,
                imgLine2,
                imgLine3;
        TextView
                tvName,
                tvAttributes,
                tvQuantity,
                tvPrice,
                tvPurchaseDate,
                tvPurchaseId,
                tvTrackingId,
                tvStatus,
                tvScheduledDeliveryDate,
                tvStatus1,
                tvStatus2,
                tvStatus3,
                tvStatus4;
        Button btnCancel;
        ProgressBar pbMain;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
