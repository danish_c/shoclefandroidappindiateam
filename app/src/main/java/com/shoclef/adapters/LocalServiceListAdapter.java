package com.shoclef.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.shoclef.R;
import com.shoclef.interfaces.LocalServiceListInterface;
import com.shoclef.objects.LocalServiceListObject;
import com.shoclef.utils.Fonts;
import com.shoclef.utils.util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class LocalServiceListAdapter extends RecyclerView.Adapter<LocalServiceListAdapter.ViewHolder> {
    private Context context;
    private LocalServiceListObject object;
    private ArrayList<LocalServiceListObject> arrayList;
    private LocalServiceListInterface anInterface;

    public LocalServiceListAdapter(Context context, LocalServiceListObject object, ArrayList<LocalServiceListObject> arrayList, LocalServiceListInterface anInterface) {
        this.context = context;
        this.object = object;
        this.arrayList = arrayList;
        this.anInterface = anInterface;
    }

    @NonNull
    @Override
    public LocalServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_service, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.cvParentService = view.findViewById(R.id.cv_parent_service);
        viewHolder.ivMainService = view.findViewById(R.id.iv_main_service);
        viewHolder.tvTitleService = view.findViewById(R.id.tv_title_service);
        viewHolder.tvLocationService = view.findViewById(R.id.tv_location_service);
        viewHolder.tvAvailablityDaysService = view.findViewById(R.id.tv_availablity_days_service);
        viewHolder.tvRatesService = view.findViewById(R.id.tv_rates_service);
        viewHolder.pbService = view.findViewById(R.id.pb_service);
        viewHolder.btnViewService = view.findViewById(R.id.btn_view_service);
        viewHolder.btnBookService = view.findViewById(R.id.btn_book_service);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final LocalServiceListAdapter.ViewHolder holder, final int position) {
        object = arrayList.get(position);
        final String serviceId = object.getServiceId();
        final String serviceImageUrl = object.getServiceImageUrl();
        final String serviceTitle = object.getServiceTitle();
        final String serviceLocation = object.getServiceLocation();
        final String serviceAvailableDays = object.getServiceAvailableDays();
        final String serviceTime = object.getServiceTime();
        final String servicePrice = object.getServicePrice();

        Picasso.get().load(serviceImageUrl).error(R.drawable.ic_logo).into(holder.ivMainService, new Callback() {
            @Override
            public void onSuccess() {
                holder.pbService.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                util.showToast("There's a problem in loading the images", context);
            }
        });
        holder.tvTitleService.setText(serviceTitle);
        holder.tvLocationService.setText(serviceLocation);
        holder.tvAvailablityDaysService.setText("Service Available on: " + serviceAvailableDays + "\n" + serviceTime);
        holder.tvRatesService.setText("Service Rate: " + servicePrice);

        holder.btnBookService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                anInterface.onBookingService(serviceId, serviceTitle, arrayList.get(position).getJsonObject());
            }
        });

        holder.btnViewService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                anInterface.onClick(serviceId, serviceTitle, arrayList.get(position).getJsonObject());
            }
        });
        setFonts(holder);
    }

    private void setFonts(ViewHolder holder) {
        Typeface light = Fonts.getLight(context);
        Typeface regular = Fonts.getRegular(context);
        Typeface medium = Fonts.getMedium(context);
        holder.tvTitleService.setTypeface(medium);
        holder.tvLocationService.setTypeface(regular);
        holder.tvAvailablityDaysService.setTypeface(regular);
        holder.tvRatesService.setTypeface(medium);
        holder.btnViewService.setTypeface(regular);
        holder.btnBookService.setTypeface(regular);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvParentService;
        ImageView ivMainService;
        TextView tvTitleService, tvLocationService, tvAvailablityDaysService, tvRatesService;
        ProgressBar pbService;
        Button btnViewService, btnBookService;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
