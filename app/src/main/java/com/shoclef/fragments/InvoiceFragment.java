package com.shoclef.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.shoclef.R;
import com.shoclef.adapters.InvoiceAdapter;
import com.shoclef.interfaces.InvoiceInterface;
import com.shoclef.objects.InvoiceObject;
import com.shoclef.utils.ApiClient;
import com.shoclef.utils.Fonts;
import com.shoclef.utils.LoggerClass;
import com.shoclef.utils.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvoiceFragment extends Fragment {

    private View view;
    private Context context;
    private String TAG = this.getClass().getSimpleName();
    private RequestQueue queue;
    private ProgressBar pbInvoice;
    private LinearLayout llErrorState, llEmptyState;
    private TextView tvLabelEmptyState,tvLabel1,tvLabel2;
    private Button btnContinueShopping;
    //Invoice RecyclerView
    private RecyclerView rvInvoice;
    private InvoiceObject invoiceObject;
    private ArrayList<InvoiceObject> invoiceArrayList = new ArrayList<>();
    private RecyclerView.Adapter invoiceAdapter;

    public InvoiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_invoice, container, false);
        initialise();

        return view;
    }

    private void initialise() {
        context = this.getContext();
        if (context != null) {
            queue = Volley.newRequestQueue(context);
        }
        assignViews();
    }

    private void assignViews() {
        rvInvoice = view.findViewById(R.id.rv_invoice);
        pbInvoice = view.findViewById(R.id.pb_invoice);
        llErrorState = view.findViewById(R.id.ll_error_state_invoice);
        llEmptyState = view.findViewById(R.id.ll_empty_state_invoice);

        tvLabelEmptyState = view.findViewById(R.id.tv_label_invoice_empty_state);
        tvLabel1=view.findViewById(R.id.tv_label_1_invoice);
                tvLabel2=view.findViewById(R.id.tv_label_2_invoice);
        btnContinueShopping = view.findViewById(R.id.btn_load_all_invoice);
        setFonts();
        setListeners();
        setUpInvoiceRecyclerView();
    }

    private void setListeners() {
        btnContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(getActivity()).finish();
            }
        });

    }

    private void setFonts() {
        tvLabelEmptyState.setTypeface(Fonts.getMedium(context));
        tvLabel1.setTypeface(Fonts.getMedium(context));
        tvLabel2.setTypeface(Fonts.getRegular(context));

    }

    private void setUpInvoiceRecyclerView() {
        invoiceAdapter = new InvoiceAdapter(context, invoiceObject, invoiceArrayList, new InvoiceInterface() {
            @Override
            public void onClick(String imageUrl, String productName, String productAttributes, String productQuantity, String productPrice, String productCurrency, String productPurchaseDate, String productTrackingId, String productStatus, String productScheduledDelivery, String productShippingStatus) {
                util.showToast(productName, context);
            }
        });
        RecyclerView.LayoutManager invoiceLayoutManager = new LinearLayoutManager(getActivity());
        rvInvoice.setLayoutManager(invoiceLayoutManager);
        rvInvoice.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL));
        invoiceAdapter.setHasStableIds(true);
        rvInvoice.setAdapter(invoiceAdapter);
        invoiceArrayList.clear();
        hitGetInvoiceListApi();

    }

    private void hitGetInvoiceListApi() {
        final String methodName = "hitGetInvoiceListApi";
        final String url = ApiClient.getInvoiceList + "12345";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    LoggerClass.i(methodName, url);
                    LoggerClass.LogApiResponse(TAG, methodName, jsonObject);
                    String status = jsonObject.getString("status");
                    if (status.equals("Sucess")) {
                        String data = jsonObject.getString("data");
                        JSONArray jsonArray = new JSONArray(data);
                        if (jsonArray.length() == 0) {
                            llEmptyState.setVisibility(View.VISIBLE);
                            pbInvoice.setVisibility(View.GONE);
                            llErrorState.setVisibility(View.GONE);
                        } else {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String imageUrl = getProductImageUrl(jsonObject1);
                                String productName = jsonObject1.getString("title");
                                String productAttributes = getAttributes(jsonObject1.getString("Products_Attributes"));
                                String invoice_no = jsonObject1.getString("invoice_no");
                                String productQuantity = jsonObject1.getString("shipped_qty");
                                String productPrice = jsonObject1.getString("total");
                                String productCurrency = jsonObject1.getString("currency_code");
                                String productPurchaseDate = jsonObject1.getString("Purchase_Date");
                                String productTrackingId = jsonObject1.getString("product_id");
                                String productStatus = "Confirm";
                                String productScheduledDelivery = jsonObject1.getString("scheduled_Delivery_Date");
                                String productShippingStatus = jsonObject1.getString("shippment_status_id");
                                invoiceObject = new InvoiceObject(imageUrl, productName, invoice_no, productAttributes, productQuantity, productPrice, productCurrency, productPurchaseDate, productTrackingId, productStatus, productScheduledDelivery, productShippingStatus);
                                invoiceArrayList.add(invoiceObject);
                                invoiceAdapter.notifyDataSetChanged();
                            }
                        }
                        pbInvoice.setVisibility(View.GONE);
                        llErrorState.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    pbInvoice.setVisibility(View.GONE);
                    llErrorState.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pbInvoice.setVisibility(View.GONE);
                llErrorState.setVisibility(View.VISIBLE);
            }
        });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);

    }

    private String getProductImageUrl(JSONObject jsonObject) {
        String productImageUrl;
        try {
            JSONArray jsonArray1 = new JSONArray(jsonObject.getString("productImages"));
            if (jsonArray1.length() > 0) {
                productImageUrl = jsonArray1.getString(0);
            } else {
                productImageUrl = "Error";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            productImageUrl = "Error";
        }

        return productImageUrl;
    }

    private String getAttributes(String attributesObject) {
        String attributes;
        try {
            JSONObject jsonObject = new JSONObject(attributesObject);
            String product_color = jsonObject.getString("product_color");
            String product_size = jsonObject.getString("product_size");
            attributes = product_color + "," + product_size;
        } catch (JSONException e) {
            e.printStackTrace();
            attributes = "Error";
        }
        return attributes;

    }
}