package com.shoclef.fragments;


import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.shoclef.R;
import com.shoclef.adapters.goods.GoodsGalleryAdapter;
import com.shoclef.interfaces.goods.GoodsGalleryInterface;
import com.shoclef.objects.goods.GoodsGalleryObject;
import com.shoclef.utils.LoggerClass;
import com.shoclef.utils.util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoodsProductDetailsFragment extends Fragment {
    private View view;
    private Context context;
    private String TAG = this.getClass().getSimpleName(), userSelectedColor, userSelectedSize, initialDiscountedPrice, initialOriginalPrice, currency_code;
    private JSONObject jsonObject = null;
    private ImageView ivMain;
    private ProgressBar pbMain;
    private TextView tvTitle, tvMinus, tvQty, tvPlus, tvDiscountedPrice, tvOriginalPrice;
    private ArrayList color = new ArrayList();
    private ArrayList size = new ArrayList();
    private Spinner spColor, spSize;
    private int quantity = 1;
    private Button btnAddToCart;
    //Gallery RecyclerView
    private RecyclerView rvGallery;
    private GoodsGalleryObject goodsGalleryObject;
    private ArrayList<GoodsGalleryObject> goodsGalleryArrayList = new ArrayList<>();
    private RecyclerView.Adapter goodsGalleryAdapter;


    public GoodsProductDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_goods_product_details, container, false);
        initialise();
        return view;
    }

    private void initialise() {
        context = this.getContext();
        if (getArguments() != null) {
            try {
                jsonObject = new JSONObject(Objects.requireNonNull(getArguments().getString("jsonObject")));
                LoggerClass.LogJson(TAG, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        assignViews();
    }

    private void assignViews() {
        ivMain = view.findViewById(R.id.iv_main_goods_details);
        pbMain = view.findViewById(R.id.pb_goods_details_image_loader);
        tvTitle = view.findViewById(R.id.tv_title_goods_details);
        tvMinus = view.findViewById(R.id.tv_minus_cart);
        tvQty = view.findViewById(R.id.tv_qty_cart);
        tvPlus = view.findViewById(R.id.tv_plus_cart);
        rvGallery = view.findViewById(R.id.rv_gallery);
        spSize = view.findViewById(R.id.sp_size);
        spColor = view.findViewById(R.id.sp_color);
        tvDiscountedPrice = view.findViewById(R.id.tv_discounted_price_goods_details);
        tvOriginalPrice = view.findViewById(R.id.tv_original_price_goods_details);
        btnAddToCart = view.findViewById(R.id.btn_add_to_cart);
        setupGalleryRecyclerView();
        setListeners();
    }

    private void setListeners() {
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quantity > 1) {
                    quantity--;
                    tvDiscountedPrice.setText(String.format("%.2f", Double.parseDouble(tvDiscountedPrice.getText().toString().replace("USD", "").trim()) - Double.parseDouble(initialDiscountedPrice.replace("USD", "").trim())) + " " + currency_code);
                    tvOriginalPrice.setText(String.format("%.2f", Double.parseDouble(tvOriginalPrice.getText().toString().replace("USD", "").trim()) - Double.parseDouble(initialOriginalPrice.replace("USD", "").trim())) + " " + currency_code);
                }
                tvQty.setText(String.valueOf(quantity));

            }
        });
        tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity++;
                tvQty.setText(String.valueOf(quantity));
                tvDiscountedPrice.setText(String.format("%.2f", Double.parseDouble(initialDiscountedPrice.replace("USD", "").trim()) * Double.parseDouble(tvQty.getText().toString().trim())) + " " + currency_code);
                tvOriginalPrice.setText(String.format("%.2f", Double.parseDouble(initialOriginalPrice.replace("USD", "").trim()) * Double.parseDouble(tvQty.getText().toString().trim())) + " " + currency_code);
            }
        });
        tvQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                quantity = Integer.parseInt(tvQty.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void getProductDetails(JSONObject jsonObject) {
        try {
            tvTitle.setText(jsonObject.getString("title"));
            tvDiscountedPrice.setText(getproductDiscountedPrice(jsonObject));
            tvOriginalPrice.setText(getproductOriginalPrice(jsonObject));
            currency_code = getCurrencyCode(jsonObject);
            initialDiscountedPrice = getproductDiscountedPrice(jsonObject);
            initialOriginalPrice = getproductOriginalPrice(jsonObject);
            tvOriginalPrice.setPaintFlags(tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            if (getproductDiscountedPrice(jsonObject).equals(getproductOriginalPrice(jsonObject))) {
                tvOriginalPrice.setVisibility(View.GONE);
            }
            Picasso.get().load(getProductImageUrl(jsonObject)).error(R.drawable.ic_logo).into(ivMain, new Callback() {
                @Override
                public void onSuccess() {
                    pbMain.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    util.showToast(String.valueOf(e), context);
                }
            });
            JSONArray jsonArray1 = new JSONArray(jsonObject.getString("productImages"));
            if (jsonArray1.length() <= 1) {
                rvGallery.setVisibility(View.GONE);
            } else {
                for (int j = 0; j < jsonArray1.length(); j++) {
                    LoggerClass.i(TAG, jsonArray1.getString(j));
                    goodsGalleryObject = new GoodsGalleryObject(jsonArray1.getString(j));
                    goodsGalleryArrayList.add(goodsGalleryObject);
                    goodsGalleryAdapter.notifyDataSetChanged();
                }
            }
            if (String.valueOf(jsonObject).contains("attributes")) {
                String attributes = jsonObject.getString("attributes");
                JSONArray attributesArray = new JSONArray(attributes);
                LoggerClass.i("getProductDetails", attributesArray.toString());

                for (int l = 0; l < attributesArray.length(); l++) {
                    JSONObject jsonObject3 = attributesArray.getJSONObject(l);
                    String name = jsonObject3.getString("name");
                    if (attributesArray.toString().contains("Color")) {
                        if (jsonObject3.toString().contains("Color")) {
                            color.clear();
                            if (name.equals("Color")) {
                                String value = jsonObject3.getString("value");
                                LoggerClass.i("0-0-0-", value);
                                String[] val = value.split(",");
                                color.add(0, "Select Color");
                                Collections.addAll(color, val);
                                setupColorAttribute();
                            } else {
                                String value = "NA";
                                LoggerClass.i("0-0-0-", value);
                                String[] val = value.split(",");
                                Collections.addAll(color, val);
                                setupColorAttribute();
                            }
                        }
                    } else {
                        color.clear();
                        String value = "NA";
                        LoggerClass.i("0-0-0-", value);
                        String[] val = value.split(",");
                        Collections.addAll(color, val);
                        setupColorAttribute();
                    }
                    if (attributesArray.toString().contains("Size")) {
                        if (jsonObject3.toString().contains("Size")) {
                            size.clear();
                            if (name.equals("Size")) {
                                String value = jsonObject3.getString("value");
                                LoggerClass.i("0-0-0-", value);
                                String[] val = value.split(",");
                                size.add(0, "Select Size");
                                Collections.addAll(size, val);
                                setupSizeAttribute();
                            } else {
                                String value = "NA";
                                LoggerClass.i("0-0-0-", value);
                                String[] val = value.split(",");
                                Collections.addAll(size, val);
                                setupSizeAttribute();
                            }
                        }
                    } else {
                        size.clear();
                        String value = "NA";
                        LoggerClass.i("0-0-0-", value);
                        String[] val = value.split(",");
                        Collections.addAll(size, val);
                        setupSizeAttribute();
                    }

                }
            } else {
                size.clear();
                String sizevalue = "NA";
                LoggerClass.i("0-0-0-", sizevalue);
                String[] sizevalueval = sizevalue.split(",");
                Collections.addAll(size, sizevalueval);
                setupSizeAttribute();
                color.clear();
                String value = "NA";
                LoggerClass.i("0-0-0-", value);
                String[] val = value.split(",");
                Collections.addAll(color, val);
                setupColorAttribute();
            }


        } catch (JSONException e) {
            e.printStackTrace();
            LoggerClass.e("getProductDetails", String.valueOf(e));
        }
    }

    private void setupGalleryRecyclerView() {
        goodsGalleryAdapter = new GoodsGalleryAdapter(context, goodsGalleryObject, goodsGalleryArrayList, new GoodsGalleryInterface() {
            @Override
            public void onClick(String imageUrl) {
                pbMain.setVisibility(View.VISIBLE);
                Picasso.get().load(imageUrl).error(R.drawable.ic_logo).into(ivMain, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbMain.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        util.showToast("Something went Wrong please try again later", context);
                    }
                });

            }
        });
        RecyclerView.LayoutManager galleryLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rvGallery.setLayoutManager(galleryLayoutManager);
        goodsGalleryAdapter.setHasStableIds(true);
        rvGallery.setAdapter(goodsGalleryAdapter);
        getProductDetails(jsonObject);

    }

    private void setupSizeAttribute() {
        ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, size);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSize.setAdapter(aa);

        spSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                userSelectedSize = String.valueOf(size.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupColorAttribute() {
        ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, color);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spColor.setAdapter(aa);

        spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                userSelectedColor = String.valueOf(color.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private String getproductDiscountedPrice(JSONObject jsonobj) {
        String productDiscountedPrice;
        if (String.valueOf(jsonobj).contains("prices")) {
            try {
                String prices = jsonobj.getString("prices");
                JSONArray jsonArray = new JSONArray(prices);
                String jsonString = jsonArray.getString(0);
                JSONObject object = new JSONObject(jsonString);
                String originalPrice = object.getString("originalPrice");
                String discountedPrice = object.getString("discountedPrice");
                JSONObject object1 = new JSONObject(discountedPrice);
                productDiscountedPrice = object1.getString("value") + " " + object1.getString("currency");
            } catch (JSONException e) {
                productDiscountedPrice = "Error";
                util.showToast(String.valueOf(e), context);
                e.printStackTrace();
            }
        } else {
            productDiscountedPrice = "Error";
        }
        return productDiscountedPrice;
    }

    private String getproductOriginalPrice(JSONObject jsonobj) {
        String productOriginalPrice;
        if (String.valueOf(jsonobj).contains("prices")) {
            try {
                String prices = jsonobj.getString("prices");
                JSONArray jsonArray = new JSONArray(prices);
                String jsonString = jsonArray.getString(0);
                JSONObject object = new JSONObject(jsonString);
                String originalPrice = object.getString("originalPrice");
                JSONObject object1 = new JSONObject(originalPrice);
                productOriginalPrice = object1.getString("value") + " " + object1.getString("currency");
            } catch (JSONException e) {
                productOriginalPrice = "Error";
                util.showToast(String.valueOf(e), context);
                e.printStackTrace();
            }
        } else {
            productOriginalPrice = "Error";
        }
        return productOriginalPrice;
    }

    private String getCurrencyCode(JSONObject jsonobj) {
        String CurrencyCode;
        if (String.valueOf(jsonobj).contains("prices")) {
            try {
                String prices = jsonobj.getString("prices");
                JSONArray jsonArray = new JSONArray(prices);
                String jsonString = jsonArray.getString(0);
                JSONObject object = new JSONObject(jsonString);
                String discountedPrice = object.getString("discountedPrice");
                JSONObject object1 = new JSONObject(discountedPrice);
                CurrencyCode = object1.getString("currency");
            } catch (JSONException e) {
                CurrencyCode = "Error";
                util.showToast(String.valueOf(e), context);
                e.printStackTrace();
            }
        } else {
            CurrencyCode = "Error";
        }
        return CurrencyCode;
    }

    private String getProductImageUrl(JSONObject jsonObject) {
        String productImageUrl;
        try {
            JSONArray jsonArray1 = new JSONArray(jsonObject.getString("productImages"));
            if (jsonArray1.length() > 0) {
                productImageUrl = jsonArray1.getString(0);
            } else {
                productImageUrl = "Error";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            productImageUrl = "Error";
        }

        return productImageUrl;
    }

}
