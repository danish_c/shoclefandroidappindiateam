package com.shoclef.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.shoclef.R;
import com.shoclef.activities.FragmentActivity;
import com.shoclef.adapters.LocalServiceListAdapter;
import com.shoclef.adapters.OnlineServiceListAdapter;
import com.shoclef.adapters.goods.GoodsCategoryAdapter;
import com.shoclef.adapters.goods.GoodsListAdapter;
import com.shoclef.interfaces.LocalServiceListInterface;
import com.shoclef.interfaces.OnlineServiceListInterface;
import com.shoclef.interfaces.goods.GoodsCategoryInterface;
import com.shoclef.interfaces.goods.GoodsListInterface;
import com.shoclef.objects.LocalServiceListObject;
import com.shoclef.objects.OnlineServiceListObject;
import com.shoclef.objects.goods.GoodsCategoryObject;
import com.shoclef.objects.goods.GoodsListObject;
import com.shoclef.utils.ApiClient;
import com.shoclef.utils.Fonts;
import com.shoclef.utils.LoggerClass;
import com.shoclef.utils.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotDealsDashboardFragment extends Fragment {
    private View view;
    private Context context;
    private String TAG = this.getClass().getSimpleName(), category = "";
    private StringRequest stringRequest;
    private RequestQueue queue;
    private ProgressBar pbLoaderGoods, pbLoaderLocalService, pbLoaderOnlineService;
    private int page = 1, goods = 0, localService = 1, onlineService = 2;
    private LinearLayout llEmptyStateGoods, llErrorStateGoods, llEmptyStateLocalService, llErrorStateLocalService, llEmptyStateOnlineService, llErrorStateOnlineService;
    private Button btnLoadAllProducts;
    private TabLayout tabHotDeals;
    private ConstraintLayout clGoods, clLocalService, clOnlineService;
    //Goods Category RecyclerView
    private RecyclerView rvGoodsCategory;
    private GoodsCategoryObject goodsCategoryObject;
    private ArrayList<GoodsCategoryObject> goodsCategoryArrayList = new ArrayList<>();
    private RecyclerView.Adapter goodsCategoryAdapter;
    //Goods List Recyclerview
    private SwipeRefreshLayout srlGoodsList;
    private RecyclerView rvGoodsList;
    private GoodsListObject goodsListObject;
    private ArrayList<GoodsListObject> goodsListArrayList = new ArrayList<>();
    private RecyclerView.Adapter goodsListAdapter;
    //Local Service Recyclerview
    private SwipeRefreshLayout srlLocalService;
    private RecyclerView rvLocalService;
    private LocalServiceListObject localServiceObject;
    private ArrayList<LocalServiceListObject> localServiceArrayList = new ArrayList<>();
    private RecyclerView.Adapter localServiceAdapter;
    //online Service Recyclerview
    private SwipeRefreshLayout srlonlineService;
    private RecyclerView rvonlineService;
    private OnlineServiceListObject onlineServiceObject;
    private ArrayList<OnlineServiceListObject> onlineServiceArrayList = new ArrayList<>();
    private RecyclerView.Adapter onlineServiceAdapter;


    public HotDealsDashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_hot_deals_dashboard, container, false);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        queue = Volley.newRequestQueue(Objects.requireNonNull(context));
        assignViews();

    }

    private void assignViews() {
        tabHotDeals = view.findViewById(R.id.tab_hot_deals);
        rvGoodsCategory = view.findViewById(R.id.rv_goods_category);
        rvGoodsList = view.findViewById(R.id.rv_goods_list);
        pbLoaderGoods = view.findViewById(R.id.pb_loader_goods);
        pbLoaderLocalService = view.findViewById(R.id.pb_loader_local_service);
        pbLoaderOnlineService = view.findViewById(R.id.pb_loader_online_service);
        llEmptyStateGoods = view.findViewById(R.id.ll_empty_state_goods);
        btnLoadAllProducts = view.findViewById(R.id.btn_load_all_products);
        srlGoodsList = view.findViewById(R.id.srl_goods_list);
        llErrorStateGoods = view.findViewById(R.id.ll_error_state_goods);
        llEmptyStateOnlineService = view.findViewById(R.id.ll_empty_state_online_Service);
        llErrorStateOnlineService = view.findViewById(R.id.ll_error_state_online_service);
        rvonlineService = view.findViewById(R.id.rv_online_service_list);
        clGoods = view.findViewById(R.id.cl_goods);
        clLocalService = view.findViewById(R.id.cl_local_service);
        clOnlineService = view.findViewById(R.id.cl_online_service);
        srlLocalService = view.findViewById(R.id.srl_local_service_list);
        rvLocalService = view.findViewById(R.id.rv_local_service_list);
        llEmptyStateLocalService = view.findViewById(R.id.ll_empty_state_local_Service);
        llErrorStateLocalService = view.findViewById(R.id.ll_error_state_local_service);
        TextView tvLabelEmptyState = view.findViewById(R.id.tv_label_product_empty_state);
        TextView tvLabel1ErrorState = view.findViewById(R.id.tv_label_1);
        TextView tvLabel2ErrorState = view.findViewById(R.id.tv_label_2);
        tvLabelEmptyState.setTypeface(Fonts.getMedium(context));
        tvLabel1ErrorState.setTypeface(Fonts.getMedium(context));
        tvLabel2ErrorState.setTypeface(Fonts.getMedium(context));
        setupCategoryRecyclerView();
        setupGoodsListRecyclerView();
        setupLocalServiceListRecyclerView();
        setupOnlineServiceListRecyclerView();
        setListeners();
        setupTabs();
    }

    private void setupOnlineServiceListRecyclerView() {
        onlineServiceAdapter = new OnlineServiceListAdapter(context, onlineServiceObject, onlineServiceArrayList, new OnlineServiceListInterface() {
            @Override
            public void onClick(String serviceId, String serviceName, JSONObject jsonObject) {
                util.showToast(serviceId, context);
            }

            @Override
            public void onBookingService(String serviceId, String serviceName, JSONObject jsonObject) {
                util.showToast(serviceId, context);
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvonlineService.setLayoutManager(layoutManager);
        rvonlineService.setAdapter(onlineServiceAdapter);
        hitGetOnlineServiceListApi();
    }

    private void hitGetOnlineServiceListApi() {
        final String methodName = "hitGetOnlineServiceListApi";
        final String url = ApiClient.onlineServiceDetail;
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    LoggerClass.i(methodName, url);
                    LoggerClass.LogApiResponse(TAG, methodName, jsonObject);
                    String onlineServicesArray = jsonObject.getString("data");
                    JSONArray jsonArray = new JSONArray(onlineServicesArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonobj = jsonArray.getJSONObject(i);
                        String serviceId = jsonobj.getString("service_list_id");
                        String serviceImageUrl = jsonobj.getString("cover_imagepath");
                        String serviceTitle = jsonobj.getString("service_name");
                        String serviceLocation = jsonobj.getString("address");
                        String serviceAvailableDays = jsonobj.getString("availability_days");
                        String available_starttime = returnTime(jsonobj.getString("available_starttime"));
                        String available_endtime = returnTime(jsonobj.getString("available_endtime"));
                        String serviceTime = available_starttime + " - " + available_endtime;
                        String original_price = jsonobj.getString("original_price");
                        String offer_price = jsonobj.getString("offer_price");
                        String currency_code = " " + jsonobj.getString("currency_code");
                        String price;
                        if (original_price.equals(offer_price)) {
                            price = offer_price + currency_code;
                        } else {
                            price = original_price + " - " + offer_price + currency_code;
                        }
                        onlineServiceObject = new OnlineServiceListObject(
                                serviceId,
                                serviceImageUrl,
                                serviceTitle,
                                serviceLocation,
                                serviceAvailableDays,
                                serviceTime,
                                price,
                                jsonobj);
                        onlineServiceArrayList.add(onlineServiceObject);
                        onlineServiceAdapter.notifyDataSetChanged();
                        pbLoaderOnlineService.setVisibility(View.GONE);
                        llEmptyStateOnlineService.setVisibility(View.GONE);
                        llErrorStateOnlineService.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoggerClass.e(methodName, String.valueOf(e));
                    llEmptyStateOnlineService.setVisibility(View.VISIBLE);
                    pbLoaderOnlineService.setVisibility(View.GONE);
                    llErrorStateOnlineService.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llEmptyStateOnlineService.setVisibility(View.VISIBLE);
                pbLoaderOnlineService.setVisibility(View.GONE);
                llErrorStateOnlineService.setVisibility(View.GONE);
            }
        });
        int socketTimeout = 30000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    }

    private void setupLocalServiceListRecyclerView() {
        localServiceAdapter = new LocalServiceListAdapter(context, localServiceObject, localServiceArrayList, new LocalServiceListInterface() {
            @Override
            public void onClick(String serviceId, String serviceName, JSONObject jsonObject) {
                util.showToast(serviceId, context);
            }

            @Override
            public void onBookingService(String serviceId, String serviceName, JSONObject jsonObject) {
                util.showToast(serviceId, context);
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvLocalService.setLayoutManager(layoutManager);
        rvLocalService.setAdapter(localServiceAdapter);
        hitGetLocalServiceListApi();
    }

    private void hitGetLocalServiceListApi() {
        final String methodName = "hitGetLocalServiceListApi";
        final String url = ApiClient.localServiceDetail;
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    LoggerClass.i(methodName, url);
                    LoggerClass.LogApiResponse(TAG, methodName, jsonObject);
                    String localServicesArray = jsonObject.getString("data");
                    JSONArray jsonArray = new JSONArray(localServicesArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonobj = jsonArray.getJSONObject(i);
                        String serviceId = jsonobj.getString("service_list_id");
                        String serviceImageUrl = jsonobj.getString("cover_imagepath");
                        String serviceTitle = jsonobj.getString("service_name");
                        String serviceLocation = jsonobj.getString("address");
                        String serviceAvailableDays = jsonobj.getString("availability_days");
                        String available_starttime = returnTime(jsonobj.getString("available_starttime"));
                        String available_endtime = returnTime(jsonobj.getString("available_endtime"));
                        String serviceTime = available_starttime + " - " + available_endtime;
                        String original_price = jsonobj.getString("original_price");
                        String offer_price = jsonobj.getString("offer_price");
                        String currency_code = " " + jsonobj.getString("currency_code");
                        String price;
                        if (original_price.equals(offer_price)) {
                            price = offer_price + currency_code;
                        } else {
                            price = original_price + " - " + offer_price + currency_code;
                        }
                        localServiceObject = new LocalServiceListObject(
                                serviceId,
                                serviceImageUrl,
                                serviceTitle,
                                serviceLocation,
                                serviceAvailableDays,
                                serviceTime,
                                price,
                                jsonobj);
                        localServiceArrayList.add(localServiceObject);
                        localServiceAdapter.notifyDataSetChanged();
                        pbLoaderLocalService.setVisibility(View.GONE);
                        llEmptyStateLocalService.setVisibility(View.GONE);
                        llErrorStateLocalService.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoggerClass.e(methodName, String.valueOf(e));
                    llErrorStateLocalService.setVisibility(View.VISIBLE);
                    pbLoaderLocalService.setVisibility(View.GONE);
                    llEmptyStateLocalService.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llErrorStateLocalService.setVisibility(View.VISIBLE);
                pbLoaderLocalService.setVisibility(View.GONE);
                llEmptyStateLocalService.setVisibility(View.GONE);
            }
        });
        int socketTimeout = 30000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);

    }

    private String returnTime(String time) {
        String returntime;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            final Date dateObj = sdf.parse(time);
            System.out.println(dateObj);
            System.out.println(new SimpleDateFormat("hh:mm a").format(dateObj));
            returntime = new SimpleDateFormat("hh:mm a").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
            returntime = "Error";
        }
        return returntime;
    }


    private void setupTabs() {
        setDefaultTab(goods);
        bindWidgetsWithAnEvent();
    }

    private void bindWidgetsWithAnEvent() {
        tabHotDeals.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Objects.requireNonNull(tabHotDeals.getTabAt(tab.getPosition())).select();
                changeTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                changeTab(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                changeTab(tab.getPosition());
            }
        });
    }

    private void setDefaultTab(int tabPosition) {
        changeTab(tabPosition);
        Objects.requireNonNull(tabHotDeals.getTabAt(tabPosition)).select();
    }

    private void changeTab(int tabPosition) {
        if (tabPosition == goods) {
            //Set All Goods methods  here
            clGoods.setVisibility(View.VISIBLE);
            clLocalService.setVisibility(View.GONE);
            clOnlineService.setVisibility(View.GONE);

        } else if (tabPosition == localService) {
//            setFragment(new LocalServiceFragment());
            clGoods.setVisibility(View.GONE);
            clLocalService.setVisibility(View.VISIBLE);
            clOnlineService.setVisibility(View.GONE);

        } else if (tabPosition == onlineService) {
//            setFragment(new OnlineServiceFragment());
            clGoods.setVisibility(View.GONE);
            clLocalService.setVisibility(View.GONE);
            clOnlineService.setVisibility(View.VISIBLE);

        }
    }

    private void setListeners() {
        btnLoadAllProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goodsListArrayList.clear();
                goodsListAdapter.notifyDataSetChanged();
                pbLoaderGoods.setVisibility(View.VISIBLE);
                llEmptyStateGoods.setVisibility(View.GONE);
                hitGetGoodsListApi(page, "20", "");
            }
        });
        srlGoodsList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                goodsListArrayList.clear();
                goodsListAdapter.notifyDataSetChanged();
                pbLoaderGoods.setVisibility(View.VISIBLE);
                hitGetGoodsListApi(page, "20", category);
            }
        });
        srlLocalService.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onlineServiceArrayList.clear();
                onlineServiceAdapter.notifyDataSetChanged();
                pbLoaderLocalService.setVisibility(View.VISIBLE);
                hitGetLocalServiceListApi();
            }
        });
    }

    private void setupGoodsListRecyclerView() {
        goodsListAdapter = new GoodsListAdapter(context, goodsListObject, goodsListArrayList, new GoodsListInterface() {
            @Override
            public void onClick(final String productId, String productName, final JSONObject jsonObject) {

                LoggerClass.LogJson(TAG, jsonObject);
                Intent intent = new Intent(getActivity(), FragmentActivity.class);
                intent.putExtra("fragmentName", "Product Detail");
                intent.putExtra("productId", productId);
                intent.putExtra("jsonObject", String.valueOf(jsonObject));
                intent.putExtra("backTo", "Main");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }

            @Override
            public void onBottomReached(int position) {
                if (goodsListArrayList.size() > 19) {
                    page++;
                    pbLoaderGoods.setVisibility(View.VISIBLE);
                    hitGetGoodsListApi(page, "20", "");
                }
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        rvGoodsList.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        rvGoodsList.setAdapter(goodsListAdapter);
        goodsListArrayList.clear();
        hitGetGoodsListApi(page, "20", "");
    }

    private void hitGetGoodsListApi(int page, final String limit, String categoryId) {
        final String methodName = "hitGetGoodsListApi";
        final String url = ApiClient.productGoodsList(String.valueOf(page), limit, categoryId);
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    LoggerClass.i(methodName, url);
                    LoggerClass.LogApiResponse(TAG, methodName, jsonObject);
                    String status = jsonObject.getString("status");
                    if (status.equals("Success")) {
                        JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                        JSONObject jsonObject2 = new JSONObject(jsonObject1.getString("body"));
                        String goodsobj1 = jsonObject2.getString("data");
                        JSONObject jsonObject3 = new JSONObject(goodsobj1);
                        String goodsobj = jsonObject3.getString("resultSet");
                        JSONArray jsonArray = new JSONArray(goodsobj);
                        if (jsonArray.length() == 0) {
                            pbLoaderGoods.setVisibility(View.GONE);
                            llEmptyStateGoods.setVisibility(View.VISIBLE);
                        } else {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject4 = jsonArray.getJSONObject(i);
                                LoggerClass.i("\n\n" + TAG + "productId: ", getProductID(jsonObject4));
                                LoggerClass.i(TAG + "ProductImageUrl: ", getProductImageUrl(jsonObject4));
                                LoggerClass.i(TAG + "ProductRating: ", getproductRating(jsonObject4));
                                LoggerClass.i(TAG + "ProductName: ", getproductName(jsonObject4));
                                LoggerClass.i(TAG + "ProductDiscountedPercentage: ", getProductDiscountedPercentage(jsonObject4));
                                LoggerClass.i(TAG + "ProductDiscountedPrice: ", getproductDiscountedPrice(jsonObject4));
                                LoggerClass.i(TAG + "ProductOriginalPrice: ", getproductOriginalPrice(jsonObject4));
                                LoggerClass.i(TAG + "ProductBought: ", getproductBought(jsonObject4));
                                if (!getproductName(jsonObject4).equals("Error")) {
                                    goodsListObject = new GoodsListObject(
                                            getProductID(jsonObject4),
                                            getProductImageUrl(jsonObject4),
                                            getproductRating(jsonObject4),
                                            getproductName(jsonObject4),
                                            getProductDiscountedPercentage(jsonObject4),
                                            getproductDiscountedPrice(jsonObject4),
                                            getproductOriginalPrice(jsonObject4),
                                            getproductBought(jsonObject4),
                                            jsonObject4);
                                    goodsListArrayList.add(goodsListObject);
                                    goodsListAdapter.notifyDataSetChanged();
                                    if (!limit.equals("20")) {
                                        goodsListAdapter.notifyItemRangeChanged(Integer.parseInt(limit) - 20, goodsListArrayList.size());
                                    }
                                }
                            }
                            pbLoaderGoods.setVisibility(View.GONE);
                            llEmptyStateGoods.setVisibility(View.GONE);
                            llErrorStateGoods.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoggerClass.e(methodName, String.valueOf(e));
                    llErrorStateGoods.setVisibility(View.VISIBLE);
                    pbLoaderGoods.setVisibility(View.GONE);
                    llEmptyStateGoods.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                LoggerClass.LogApiError(getActivity(), TAG, url, methodName, error);
                llErrorStateGoods.setVisibility(View.VISIBLE);
                pbLoaderGoods.setVisibility(View.GONE);
                llEmptyStateGoods.setVisibility(View.GONE);
            }
        });
        int socketTimeout = 30000; //30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    }


    private void setupCategoryRecyclerView() {
        goodsCategoryAdapter = new GoodsCategoryAdapter(context, goodsCategoryObject, goodsCategoryArrayList, new GoodsCategoryInterface() {
            @Override
            public void onClick(String categoryId, String categoryName) {
                goodsListArrayList.clear();
                goodsListAdapter.notifyDataSetChanged();
                pbLoaderGoods.setVisibility(View.VISIBLE);
                llEmptyStateGoods.setVisibility(View.GONE);
                hitGetGoodsListApi(page, "20", categoryId);
                category = categoryId;
            }
        });
        rvGoodsCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvGoodsCategory.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL));
        goodsCategoryAdapter.setHasStableIds(true);
        rvGoodsCategory.setAdapter(goodsCategoryAdapter);
        goodsCategoryArrayList.clear();
        hitGetGoodsCategoryApi();
    }

    private void hitGetGoodsCategoryApi() {
        final String methodName = "hitGetGoodsCategoryApi";
        final String url = ApiClient.goodsCategoryList + "0";
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    LoggerClass.i(methodName, url);
                    LoggerClass.LogApiResponse(TAG, methodName, jsonObject);
                    String goodsCategory = jsonObject.getString("data");
                    JSONArray jsonArray = new JSONArray(goodsCategory);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonobj = jsonArray.getJSONObject(i);
                        String id = jsonobj.getString("provider_category_id");
                        String name = jsonobj.getString("category_name");
                        goodsCategoryObject = new GoodsCategoryObject(id, name);
                        goodsCategoryArrayList.add(goodsCategoryObject);
                        goodsCategoryAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoggerClass.e(methodName, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                LoggerClass.LogApiError(getActivity(), TAG, url, methodName, error);
            }
        });
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    }

    private String getProductID(JSONObject jsonObject) {
        String productId;
        if (String.valueOf(jsonObject).contains("id")) {
            try {
                productId = jsonObject.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
                productId = "Error";
            }
        } else {
            productId = "Error";
        }
        return productId;
    }

    private String getProductImageUrl(JSONObject jsonObject) {
        String productImageUrl;
        try {
            JSONArray jsonArray1 = new JSONArray(jsonObject.getString("productImages"));
            if (jsonArray1.length() > 0) {
                productImageUrl = jsonArray1.getString(0);
            } else {
                productImageUrl = "Error";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            productImageUrl = "Error";
        }

        return productImageUrl;
    }

    private String getproductRating(JSONObject jsonObject) {
        String productRating;
        if (String.valueOf(jsonObject).contains("reviews")) {
            try {
                JSONObject jsonObject1 = new JSONObject(jsonObject.getString("reviews"));
                productRating = jsonObject1.getString("ratings");
            } catch (JSONException e) {
                e.printStackTrace();
                productRating = "Error";
            }
        } else {
            productRating = "Error";
        }

        return productRating;
    }

    private String getproductName(JSONObject jsonobj) {
        String productName;
        if (String.valueOf(jsonobj).contains("title")) {
            try {
                productName = jsonobj.getString("title");
            } catch (JSONException e) {
                e.printStackTrace();
                productName = "Error";
            }
        } else {
            productName = "Error";
        }
        return productName;
    }

    private String getProductDiscountedPercentage(JSONObject jsonObject) {
        if (!getSmallNumber(jsonObject).equals("Error")) {
            if (!getLargeNumber(jsonObject).equals("Error")) {
                if (!getSmallNumber(jsonObject).equals(getLargeNumber(jsonObject))) {
                    double smallAmount = Double.parseDouble((getSmallNumber(jsonObject)));
                    double totalAmount = Double.parseDouble((getLargeNumber(jsonObject)));
                    int percentage = (int) (smallAmount / totalAmount * 100);
                    return String.valueOf(percentage);
                } else {
                    return "NA";
                }
            } else {
                return "NA";
            }
        } else {
            return "NA";
        }
    }

    private String getSmallNumber(JSONObject jsonobj) {
        String productDiscountedPrice;
        if (String.valueOf(jsonobj).contains("prices")) {
            try {
                String prices = jsonobj.getString("prices");
                JSONArray jsonArray = new JSONArray(prices);
                String jsonString = jsonArray.getString(0);
                JSONObject object = new JSONObject(jsonString);
                String discountedPrice = object.getString("discountedPrice");
                JSONObject object1 = new JSONObject(discountedPrice);
                productDiscountedPrice = object1.getString("value");
            } catch (JSONException e) {
                productDiscountedPrice = "Error";
//                util.showToast(e + ":getSmallNumber", context);
                e.printStackTrace();
            }
        } else {
            productDiscountedPrice = "Error";
        }
        return productDiscountedPrice;
    }

    private String getLargeNumber(JSONObject jsonobj) {
        String productOriginalPrice;
        if (String.valueOf(jsonobj).contains("prices")) {
            try {
                String prices = jsonobj.getString("prices");
                JSONArray jsonArray = new JSONArray(prices);
                String jsonString = jsonArray.getString(0);
                JSONObject object = new JSONObject(jsonString);
                String originalPrice = object.getString("originalPrice");
                JSONObject object1 = new JSONObject(originalPrice);
                productOriginalPrice = object1.getString("value");
            } catch (JSONException e) {
                productOriginalPrice = "Error";
//                util.showToast(e + ":getLargeNumber", context);
                e.printStackTrace();
            }
        } else {
            productOriginalPrice = "Error";
        }
        return productOriginalPrice;
    }

    private String getproductDiscountedPrice(JSONObject jsonobj) {
        String productDiscountedPrice;
        if (String.valueOf(jsonobj).contains("prices")) {
            try {
                String prices = jsonobj.getString("prices");
                JSONArray jsonArray = new JSONArray(prices);
                String jsonString = jsonArray.getString(0);
                JSONObject object = new JSONObject(jsonString);
                String discountedPrice = object.getString("discountedPrice");
                JSONObject object1 = new JSONObject(discountedPrice);
                productDiscountedPrice = object1.getString("value") + " " + object1.getString("currency");
            } catch (JSONException e) {
                productDiscountedPrice = "Error";
//                util.show Toast(e + ":getproductDiscountedPrice", context);
                e.printStackTrace();
            }
        } else {
            productDiscountedPrice = "Error";
        }
        return productDiscountedPrice;
    }

    private String getproductOriginalPrice(JSONObject jsonobj) {
        String productOriginalPrice;
        if (String.valueOf(jsonobj).contains("prices")) {
            try {
                String prices = jsonobj.getString("prices");
                JSONArray jsonArray = new JSONArray(prices);
                String jsonString = jsonArray.getString(0);
                JSONObject object = new JSONObject(jsonString);
                String originalPrice = object.getString("originalPrice");
                JSONObject object1 = new JSONObject(originalPrice);
                productOriginalPrice = object1.getString("value") + " " + object1.getString("currency");
            } catch (JSONException e) {
                productOriginalPrice = "Error";
//                util.showToast(e + ":getproductOriginalPrice", context);
                e.printStackTrace();
            }
        } else {
            productOriginalPrice = "Error";
        }
        return productOriginalPrice;
    }

    private String getproductBought(JSONObject jsonobj) {
        String productBought;
        if (String.valueOf(jsonobj).contains("reviews")) {
            try {
                JSONObject jsonObject1 = new JSONObject(jsonobj.getString("trade"));
                productBought = jsonObject1.getString("sold");
            } catch (JSONException e) {
                e.printStackTrace();
                productBought = "Error";
            }
        } else {
            productBought = "Error";
        }

        return productBought;
    }

}
