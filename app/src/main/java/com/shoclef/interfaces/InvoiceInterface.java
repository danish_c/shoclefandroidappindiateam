package com.shoclef.interfaces;

public interface InvoiceInterface {
    void onClick(String imageUrl, String productName, String productAttributes, String productQuantity, String productPrice, String productCurrency, String productPurchaseDate, String productTrackingId, String productStatus, String productScheduledDelivery, String productShippingStatus);
}
