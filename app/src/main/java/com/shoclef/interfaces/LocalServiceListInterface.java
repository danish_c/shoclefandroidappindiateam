package com.shoclef.interfaces;

import org.json.JSONObject;

public interface LocalServiceListInterface {
    void onClick(String serviceId, String serviceName, JSONObject jsonObject);
    void onBookingService(String serviceId, String serviceName, JSONObject jsonObject);
}
