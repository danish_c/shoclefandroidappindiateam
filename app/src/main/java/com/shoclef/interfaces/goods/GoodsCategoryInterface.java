package com.shoclef.interfaces.goods;

public interface GoodsCategoryInterface {
    void onClick(String categoryId, String categoryName);
}
