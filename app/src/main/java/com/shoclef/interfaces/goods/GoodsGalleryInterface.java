package com.shoclef.interfaces.goods;

public interface GoodsGalleryInterface {
    void onClick(String imageUrl);
}
