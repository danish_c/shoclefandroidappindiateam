package com.shoclef.interfaces.goods;

import org.json.JSONObject;

public interface GoodsListInterface {
    void onClick(String productId, String productName, JSONObject jsonObject);
    void onBottomReached(int position);


}
