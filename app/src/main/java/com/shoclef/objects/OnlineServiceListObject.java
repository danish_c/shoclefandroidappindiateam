package com.shoclef.objects;

import org.json.JSONObject;

public class OnlineServiceListObject {
    String serviceId, serviceImageUrl, serviceTitle, serviceLocation, serviceAvailableDays, serviceTime, servicePrice;
    JSONObject jsonObject;

    public OnlineServiceListObject(String serviceId, String serviceImageUrl, String serviceTitle, String serviceLocation, String serviceAvailableDays, String serviceTime, String servicePrice, JSONObject jsonObject) {
        this.serviceId = serviceId;
        this.serviceImageUrl = serviceImageUrl;
        this.serviceTitle = serviceTitle;
        this.serviceLocation = serviceLocation;
        this.serviceAvailableDays = serviceAvailableDays;
        this.serviceTime = serviceTime;
        this.servicePrice = servicePrice;
        this.jsonObject = jsonObject;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceImageUrl() {
        return serviceImageUrl;
    }

    public void setServiceImageUrl(String serviceImageUrl) {
        this.serviceImageUrl = serviceImageUrl;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getServiceLocation() {
        return serviceLocation;
    }

    public void setServiceLocation(String serviceLocation) {
        this.serviceLocation = serviceLocation;
    }

    public String getServiceAvailableDays() {
        return serviceAvailableDays;
    }

    public void setServiceAvailableDays(String serviceAvailableDays) {
        this.serviceAvailableDays = serviceAvailableDays;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
