package com.shoclef.objects.goods;

import org.json.JSONObject;

public class GoodsListObject {
    String productId, productImageUrl, productRating, productName, productDiscountedPercentage, productDiscountedPrice, productPrice, productBought;
    JSONObject jsonObject;

    public GoodsListObject(String productId, String productImageUrl, String productRating, String productName, String productDiscountedPercentage, String productDiscountedPrice, String productPrice, String productBought, JSONObject jsonObject) {
        this.productId = productId;
        this.productImageUrl = productImageUrl;
        this.productRating = productRating;
        this.productName = productName;
        this.productDiscountedPercentage = productDiscountedPercentage;
        this.productDiscountedPrice = productDiscountedPrice;
        this.productPrice = productPrice;
        this.productBought = productBought;
        this.jsonObject = jsonObject;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public String getProductRating() {
        return productRating;
    }

    public void setProductRating(String productRating) {
        this.productRating = productRating;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDiscountedPercentage() {
        return productDiscountedPercentage;
    }

    public void setProductDiscountedPercentage(String productDiscountedPercentage) {
        this.productDiscountedPercentage = productDiscountedPercentage;
    }

    public String getProductDiscountedPrice() {
        return productDiscountedPrice;
    }

    public void setProductDiscountedPrice(String productDiscountedPrice) {
        this.productDiscountedPrice = productDiscountedPrice;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductBought() {
        return productBought;
    }

    public void setProductBought(String productBought) {
        this.productBought = productBought;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
