package com.shoclef.objects.goods;

public class GoodsGalleryObject {
    String imageUrl;

    public GoodsGalleryObject(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
