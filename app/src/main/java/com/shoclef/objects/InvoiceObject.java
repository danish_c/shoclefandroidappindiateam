package com.shoclef.objects;

public class InvoiceObject {
    String imageUrl, productName, productInvoiceId, productAttributes, productQuantity, productPrice, productCurrency, productPurchaseDate, productTrackingId,
            productStatus, productScheduledDelivery, productShippingStatus;

    public InvoiceObject(String imageUrl, String productName, String productInvoiceId, String productAttributes, String productQuantity, String productPrice, String productCurrency, String productPurchaseDate, String productTrackingId, String productStatus, String productScheduledDelivery, String productShippingStatus) {
        this.imageUrl = imageUrl;
        this.productName = productName;
        this.productInvoiceId = productInvoiceId;
        this.productAttributes = productAttributes;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
        this.productCurrency = productCurrency;
        this.productPurchaseDate = productPurchaseDate;
        this.productTrackingId = productTrackingId;
        this.productStatus = productStatus;
        this.productScheduledDelivery = productScheduledDelivery;
        this.productShippingStatus = productShippingStatus;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductInvoiceId() {
        return productInvoiceId;
    }

    public void setProductInvoiceId(String productInvoiceId) {
        this.productInvoiceId = productInvoiceId;
    }

    public String getProductAttributes() {
        return productAttributes;
    }

    public void setProductAttributes(String productAttributes) {
        this.productAttributes = productAttributes;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductCurrency() {
        return productCurrency;
    }

    public void setProductCurrency(String productCurrency) {
        this.productCurrency = productCurrency;
    }

    public String getProductPurchaseDate() {
        return productPurchaseDate;
    }

    public void setProductPurchaseDate(String productPurchaseDate) {
        this.productPurchaseDate = productPurchaseDate;
    }

    public String getProductTrackingId() {
        return productTrackingId;
    }

    public void setProductTrackingId(String productTrackingId) {
        this.productTrackingId = productTrackingId;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getProductScheduledDelivery() {
        return productScheduledDelivery;
    }

    public void setProductScheduledDelivery(String productScheduledDelivery) {
        this.productScheduledDelivery = productScheduledDelivery;
    }

    public String getProductShippingStatus() {
        return productShippingStatus;
    }

    public void setProductShippingStatus(String productShippingStatus) {
        this.productShippingStatus = productShippingStatus;
    }
}
