package com.shoclef.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shoclef.R;
import com.shoclef.utils.SharedPref;

public class TermsAndConditionsActivity extends AppCompatActivity {
    private ImageView ivClose;
    private TextView tvText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        assignViews();
    }

    private void assignViews() {
        ivClose = findViewById(R.id.iv_close);
        tvText = findViewById(R.id.tv_text);

        setListeners();
        setTnc();
    }

    private void setTnc() {
        tvText.setText(Html.fromHtml(
                "<p>Last update: May 1, 2019</p>\n" +
                        "<p>Welcome to Shoclef.com. Shoclef.com is a marketplace platform for services performed by its users. Your use of our services is governed by our Terms of Use. This document further explains how our services work in details and is incorporated into the Terms of Use.</p>\n" +
                        "<p>Our Customer Support team is available 24/7 if you have any questions regarding our website or services. Contacting our Customer Support team can be performed by submitting a request here.</p>\n" +
                        "<p>Key Terms</p>\n" +
                        "<p>Sellers are users who offer and perform services through Shoclef.</p>\n" +
                        "<p>Buyers are users who purchase services on Shoclef.\u200B</p>\n" +
                        "<p>Custom Offers are exclusive proposals that a seller can create in response to specific requirements of a buyer.</p>\n" +
                        "<p>Custom Orders are requests made by a buyer to receive a Custom Offer from a seller.</p>\n" +
                        "<p>Orders are the formal agreement between a buyer and personal shopper/business/factory after a purchase was made from the Order Page.</p>\n" +
                        "<p>Order Page is where buyers and sellers communicate with each other in connection with an order.</p>\n" +
                        "<p>Disputes are disagreements experienced during an order between a buyer and seller on Shoclef.</p>\n" +
                        "<p>Revenue is the money sellers earn from completed orders.</p>\n" +
                        "<p>Sales Balance is cleared revenue from completed orders for sellers to withdraw or use to purchase items.</p>\n" +
                        "<p>Shopping Balance is shopping credit collected from cancelled orders or Shoclef promotions to be used for purchasing items.</p>\n" +
                        "<p>Service Providers are professionals who charge money for their services to registered Users.</p>\n" +
                        "<p>Observers are Users who watch live Players. Observers may pay Players tips for doing tasks.</p>\n" +
                        "<p>Players participate in live video streams and interact live with Observers and Users; complete tasks for a tip.</p>\n" +
                        "<p>Overview (Main terms, in a nutshell)</p>\n" +
                        "<p>* Only registered users may buy and sell, play, observe, post, promote on Shoclef.com. Registration is free.</p>\n" +
                        "<p>* Buyers pay Shoclef in advance to create an order by adding money to their account.</p>\n" +
                        "<p>* Orders are purchased through the Order button found on a seller&rsquo;s page or through a Custom Offer.</p>\n" +
                        "<p>* For fees and payments please read the purchasing section here and our Pricing Policy.</p>\n" +
                        "<p>* Sellers must fulfill their orders, and may not cancel orders on a regular basis or without cause. Cancelling orders will affect sellers&rsquo; reputation and status.</p>\n" +
                        "<p>* Sellers gain account statuses (Levels) based on their performance and reputation. Advanced levels provide their owners with benefits, including offering services for higher prices.</p>\n" +
                        "<p>* Users may not offer or accept payments using any method other than placing an order through Shoclef.com.</p>\n" +
                        "<p>* Buyers are granted all rights for the delivered work, unless otherwise specified by the seller on their Profile Page.&nbsp; See our &ldquo;Ownership&rdquo; and &ldquo;Commercial Use License&rdquo; sections below for more information.</p>\n" +
                        "<p>* Shoclef retains the right to use all published delivered works for Shoclef marketing and promotion purposes.</p>\n" +
                        "<p>* We care about your privacy. You can read our Privacy Policy here. The Privacy Policy is a part of these Terms of Service and incorporated herein by reference.</p>\n" +
                        "<p>Sellers</p>\n" +
                        "<p>Basics</p>\n" +
                        "<p>* Personal Shoppers. Businesses, Factories, Service Providers create requests on Shoclef to allow buyers to purchase their services.</p>\n" +
                        "<p>* Personal Shoppers may also offer Custom Offers to buyers.</p>\n" +
                        "<p>* Each order a Personal Shoppers sells and successfully completes, accredits your account with a net revenue of 50% of the purchase amount of Live minutes charged and collected.</p>\n" +
                        "<p>* Players receive net revenue of 50% of the purchase amount of Live minutes charged and collected. Players may also collect a tip and Players keep 80% of the tip. All tips are final.</p>\n" +
                        "<p>* Shoclef accredits Personal Shopper/Seller/Player once an order is completed and item/s are delivered. See our \"HYPERLINK \"https://www.fiverr.com/terms_of_service?source=footer#orders\"OrdersHYPERLINK \"https://www.fiverr.com/terms_of_service?source=footer#orders\"\" section below for a definition of a completed order.</p>\n" +
                        "<p>* Orders once placed are not able to be cancelled (for any reason). All purchases are final and binding.</p>\n" +
                        "<p>* Revenues are only made available for withdrawal from the Revenue page following&nbsp; after the order is marked as complete.</p>\n" +
                        "<p>* Any and all transaction bank fees and withdrawl fees will be charged at time of withdrawl.</p>\n" +
                        "<p>* Personal Shoppers may withdraw their revenues using one of Shoclef&rsquo;s withdrawal options.</p>\n" +
                        "<p>* The seller/personal shoppers/players's rating is calculated based on the order reviews posted by buyers/observers.&nbsp;</p>\n" +
                        "<p>* For security concerns, Shoclef may temporarily disable a seller&rsquo;s ability to withdraw revenue to prevent fraudulent or illicit activity. This may come as a result of security issues, improper behavior reported by buyers, or associating multiple Shoclef accounts to a single withdrawal provider.</p>\n" +
                        "<p>Seller/Personal Shoppers/Factories/Businesses Features</p>\n" +
                        "<p>Shoclef sellers have access to several exclusive features that help customize the way their services can be offered.</p>\n" +
                        "<p>Custom Offer</p>\n" +
                        "<p>* Sellers can also send Custom Offers addressing specific requirements of a buyer.</p>\n" +
                        "<p>* Custom Offers are defined by the seller with the exact description of the service, the price and the time expected to deliver the service.</p>\n" +
                        "<p>* Custom Offers are sent from the conversation page.</p>\n" +
                        "<p>* Services provided through Custom Offers may not violate Shoclef&rsquo;s Terms of Use.</p>\n" +
                        "<p>Shoclef.com</p>\n" +
                        "<p>* Services offered through Shoclef.com must comply with our Terms of Use.</p>\n" +
                        "<p>* Users accessing and purchasing from a Shoclef.com that are not already Users of Shoclef, will be required to register with Shoclef to create an order.</p>\n" +
                        "<p>* Sellers are required to deliver proof of completion of services in the order page. Please see our Orders section for further details.</p>\n" +
                        "<p>* Communication for handling orders should be performed on Shoclef.com, through the live shopping/order page. Users who engage and communicate off of Shoclef.com&nbsp; will not be protected by our Terms of Use.</p>\n" +
                        "<p>* Shoclef International has not verified the licenses and certifications of any Users, Persons, Trade persons, Shoppers, Buyers, Factories or Businesses, Service Providers, Players, Observers offering services on our websites and/or Apps. Shoclef International S. A. is not liable for any misinformation, misrepresentation of any Users on any of our sites. Shoclef Corporation Limited shall not be responsible or liable for any damages or other problems resulting from any and all services rendered.</p>\n" +
                        "<p>Shipping Physical Deliverables</p>\n" +
                        "<p>Some of the services on Shoclef.com&nbsp; are delivered physically (shoes, purses, collectable items, etc.). For these types of Purchases, sellers may add shipping charges. Sellers/Personal Shoppers/Businesses/Factories can add shipping charges for local shipping (within the same country) and for international shipping (anywhere else).</p>\n" +
                        "<p>* Items that require shipping costs must have physical deliverables sent to Buyers.</p>\n" +
                        "<p>* Shipping costs added to a purchase only pertains to the cost sellers require to ship physical items to Buyers.</p>\n" +
                        "<p>* Important: Buyers who purchase Items that require physical delivery, will be asked to provide a shipping address.</p>\n" +
                        "<p>* Sellers are responsible for all shipping arrangements once the buyer provides the shipping address.</p>\n" +
                        "<p>* Shoclef does not handle or guarantee shipping, tracking, quality, and condition of items or their delivery and shall not be responsible or liable for any damages or other problems resulting from shipping.</p>\n" +
                        "<p>* A tracking number is a great way to avoid disputes related to shipping. We require entering the tracking number if available in the order page when delivering your Purchases.</p>\n" +
                        "<p>* Users can contact Shoclef's customer support department for assistance here.</p>\n" +
                        "<p>Withdrawing Revenues</p>\n" +
                        "<p>* To withdraw your revenue, you must have an account with at least one of Shoclef's withdrawal methods.</p>\n" +
                        "<p>* Your Shoclef profile can be associated with only one account from each Shoclef withdrawal methods. A withdrawal provider account can be associated with only one Shoclef profile.</p>\n" +
                        "<p>* To withdraw your available revenue, you must click on the designated withdrawal provider to initiate the withdrawal process</p>\n" +
                        "<p>* Withdrawals can only be made in the amount available to you.</p>\n" +
                        "<p>* Withdrawal fees vary depending on the withdrawal method.</p>\n" +
                        "<p>* Withdrawals are final and cannot be undone. We will not be able to refund or change this process once it has begun.</p>\n" +
                        "<p>Withdrawal Methods</p>\n" +
                        "<p>Method</p>\n" +
                        "<p>Fee</p>\n" +
                        "<p>Service Availability</p>\n" +
                        "<p>Withdraw to your Shocelf Account</p>\n" +
                        "<p>2% of the sum withdrawal amount up to $1*</p>\n" +
                        "<p>For a list of Shoclef services by country click here</p>\n" +
                        "<p>Shoclef Revenue Card&trade;</p>\n" +
                        "<p>$1*within 2 days, or $3 within 2 hours.<br />For more information, see .Pricing and Fees</p>\n" +
                        "<p>Worldwide</p>\n" +
                        "<p>Local Bank Transfer (LBT)</p>\n" +
                        "<p>$3 per transfer*.<br />For more information, see Pricing and Fees.</p>\n" +
                        "<p>Worldwide</p>\n" +
                        "<p>Direct Deposit (ACH)</p>\n" +
                        "<p>$1 per transfer.<br />For more information, see Pricing and Fees.</p>\n" +
                        "<p>US only</p>\n" +
                        "<p>* Additional fees may apply based on your location and currency.</p>\n" +
                        "<p>Buyers</p>\n" +
                        "<p>Basics</p>\n" +
                        "<p>* You may not offer direct payments to sellers using payment systems outside of the Shoclef Order system.</p>\n" +
                        "<p>* Shoclef retains the right to use all publically published delivered works for Shoclef marketing and promotional purposes.</p>\n" +
                        "<p>* Buyers may request a specific service from the Post a Request feature found from the Shopping dashboard. Services requested on Shoclef must be an allowed service on Shoclef. Please click here for guidelines on approved services.</p>\n" +
                        "<p>Purchasing</p>\n" +
                        "<p>* Buyers pay Shoclef to create an order from a Seller&rsquo;s Page using the Order Now button.</p>\n" +
                        "<p>* In addition buyers can request a Custom Order which addresses specific buyer requirements, and receive a Custom Offer from sellers through the site or through Shoclef.com.</p>\n" +
                        "<p>* Items may be purchased using one of the following payment methods: Credit Card, PayPal, Venmo or Shoclef Credit.</p>\n" +
                        "<p>* Processing fees are added at the time of purchase where a buyer can review and accept the total amount requested to pay. These fees cover payment processing and administrative fees. As of September 8, 2017, the current fees assessed to the total purchase amount are $1 on purchases up to $20 and 5% on purchases above $20. When purchasing from your seller's balance (i.e. out of your account) or buyer's shopping balance (resulting from any credits or refunds) you will not be charged a processing fee. Funds returned to your balance from cancelled orders will not include processing fees paid.</p>\n" +
                        "<p>* If you have funds in your account balance, either from your Shopping or available Account balance, it will be automatically applied to your next purchase.</p>\n" +
                        "<p>* You may not offer sellers to pay, or make payment using any method other than through the Shoclef.com site. In case you have been asked to use an alternative payment method, please report it immediately to Customer Support here.</p>\n" +
                        "<p>* To protect against fraud, unauthorized transactions (such as money laundering), claims or other liabilities, we do not collect credit information; but allow our payment vendors to collect information for the purpose of collecting payments from buyers on the Site or transferring payments to sellers on the Site. We are not exposed to the payment information provided to our payment vendors, and this information is subject to the privacy policy applicable to the payment vendor. Please see our Privacy Policy for more information here.</p>\n" +
                        "<p>VAT</p>\n" +
                        "<p>This paragraph only applies to buyers located in Israel: Israeli buyers will be charged VAT in addition to the Prices shown on the page. An invoice will be provided to allow eligible users to claim back the VAT from the tax authorities.</p>\n" +
                        "<p>Orders</p>\n" +
                        "<p>Basics</p>\n" +
                        "<p>* Once payment is confirmed, your order will be created and given a unique Shoclef order number (#FO).</p>\n" +
                        "<p>* Sellers must deliver purchased items and/or proof of delivery using the Deliver Items button (located on the Order page) according to the service that was purchased and advertised on their profile page.</p>\n" +
                        "<p>* Using the Deliver Items button may not be abused by sellers to circumvent Order guidelines described in this Terms of Service. Using the &ldquo;Deliver Items&rdquo; button when an order was not fulfilled may result in a cancellation of that order after review, affect the seller&rsquo;s rating and result in a warning to seller.</p>\n" +
                        "<p>* An order is marked as complete after the order is marked as Delivered and then rated by a buyer. An order will be automatically marked as complete if not rated and no request for modification was submitted within 3 days after marked as Delivered.</p>\n" +
                        "<p>* We encourage our buyers and sellers to try and settle conflicts amongst themselves. If for any reason this fails after using the Resolution Center or if you encounter non-permitted usage on the Site, users can contact Shoclef's Customer Support department for assistance here.</p>\n" +
                        "<p>Handling Orders</p>\n" +
                        "<p>* When a buyer orders, the personal shopper/factory/business/vender is notified by email as well as notifications on the site while logged into the account.</p>\n" +
                        "<p>* Sellers/personal shoppers/factories/businesses are required to meet the delivery time they specified when creating their Accounts. Failing to do so will allow the buyer to cancel the order when an order is marked as late and may harm the seller's status.</p>\n" +
                        "<p>* Sellers must send completed files and/or proof of work using the Deliver Items button (located on the Order page) to mark the order as Delivered.</p>\n" +
                        "<p>* Users are responsible for scanning all transferred files for viruses and malware. Shoclef will not be held responsible for any damages which might occur due to site usage, use of content or files transferred.</p>\n" +
                        "<p>&nbsp;</p>\n" +
                        "<p>* Buyers may use the &ldquo;Open a Dispute\" feature located on the Account History page while an order is marked as Delivered if the delivered materials do not match the seller's description on their Account page or they do not match the requirements sent to the seller at the beginning of the order process.</p>\n" +
                        "<p>Reviews</p>\n" +
                        "<p>* Feedback reviews provided by buyers while completing an order are an essential part of Shoclef's rating system. Reviews demonstrate the buyer's overall experience with the sellers and their service. Buyers are encouraged to communicate to the seller any concerns experienced during their active order in regards to the service provided by the seller.</p>\n" +
                        "<p>* Leaving a buyer's feedback is a basic prerogative of a buyer. Feedback reviews will not be removed unless there are clear violations to our Terms of Use.</p>\n" +
                        "<p>* To prevent any misuse of our Feedback system, all feedback reviews must come from legitimate sales executed exclusively through the Shoclef platform from users within our Community. Purchases arranged, determined to artificially enhance seller ratings, or to abuse the Shoclef platform with purchases from additional accounts, will result in a permanent suspension of all related accounts.</p>\n" +
                        "<p>* Feedback comments given by buyers are publicly displayed on a Personal Shopper&rsquo;s Account page. Buyers have the option not to include a comment, but still rate the service. Cancellation of an order does not remove feedback unless mutually agreed.</p>\n" +
                        "<p>* Withholding the delivery of services, files, or information required to complete the Account service with the intent to gain favorable reviews or additional services is prohibited.</p>\n" +
                        "<p>* Responding and posting a review: Once work is delivered, the buyer has three days to respond and post a review (or 14 days with shipping). If no response is provided within the response period, the order will be considered completed.</p>\n" +
                        "<p>* Users are allowed to leave reviews on orders up to 30 days after an order is marked as complete. No new reviews may be added to an order after 30 days.</p>\n" +
                        "<p>* Sellers may not solicit the removal of feedback reviews from their buyers through mutual cancellations.</p>\n" +
                        "<p>Disputes and Cancellations</p>\n" +
                        "<p>We encourage our buyers and sellers to try and settle conflicts amongst themselves. If for any reason this fails after using the Resolution Center or if you encounter non-permitted usage on the Site, users can contact Shoclef's Customer Support department for assistance here.</p>\n" +
                        "<p>Basics</p>\n" +
                        "<p>* Order cancellations can never be performed on Shoclef.com, because all purchased items and services are final.</p>\n" +
                        "<p>* Filing a transaction dispute or reversing a payment through your payment provider or your bank is a violation to these Terms of Services. Doing so may get your account temporarily disabled to investigate possible security violations. Note: once you have filed a dispute with your payment provider, the funds will be ineligible for a refund due to our obligations towards the payment provider.</p>\n" +
                        "<p>* In the event that a buyer or seller encounters an issue related to the service provided in an order, you are encouraged to use the Site's dispute resolution tools to attempt to resolve the matter.</p>\n" +
                        "<p>* Shoclef reserves the right to cancel orders or place funds on hold for any suspected fraudulent transactions made on the Site.</p>\n" +
                        "<p>* All transfer and assignment of intellectual property to the buyer shall be subject to full payment for the purchases and the delivery may not be used if payment is cancelled for any reason.</p>\n" +
                        "<p>* Shoclef does not allow cancellations (for any reason). All the funds paid will never be refunded to the buyer&rsquo;s shopping balance unless items that require shipping are not received.</p>\n" +
                        "<p>* Revisions to delivery costs can be performed by sellers based on the seller&rsquo;s acutal costs. Proof of shipping costs are required to be verified with proof within 48 hours of shipping items.</p>\n" +
                        "<p>* Requests for revisions can be performed through the Order page while the order is marked as Delivered.</p>\n" +
                        "<p>* Requesting to gain more services from sellers beyond the agreed requirements by using the Request Revisions button is not allowed.</p>\n" +
                        "<p>Order Cancellations</p>\n" +
                        "<p>* Shoclef Corporation Limited encourages Buyers and Sellers to resolve service disputes mutually using the Dispute Transaction Button.</p>\n" +
                        "<p>* Eligibility for requests to Shoclef Corporation Limited to dispute an order will be assessed by our Customer Support team based on a number of factors, including violations to our Terms of Use, general misconduct, and improper usage of the Shoclef delivery system. See below for Order specific eligibility.</p>\n" +
                        "<p>* Orders are not eligible to be cancelled based on the quality of service/materials delivered by the seller if the service was rendered as described in the Account Information page. Buyers may rate their experience with the seller on the order page, including the overall level of service quality received.</p>\n" +
                        "<p>* Buyers must use Dispute Transaction Form to address their concerns and desired resolution related to the service provided by their seller prior to contacting Customer Support. Customer Support will not take any action against Orders where the buyers failed to inform their seller of issues related to the seller&rsquo;s service and will allow sellers to provide a resolution first. This does not include non-permitted usage of Shoclef.</p>\n" +
                        "<p>* Any non-permitted usage of Shoclef encountered during an Order, after being reviewed by our Customer Support team, may result in the order being cancelled. This includes, but not limited to; harassment, unlawful behavior, or other violations to Shoclef&rsquo;s Terms of Use.</p>\n" +
                        "<p>* Shoclef Customer Support will cancel orders based on, but not limited to, the following reasons:</p>\n" +
                        "<p>* Active orders (after the buyer submits their requirements and before the seller delivers on Shoclef)</p>\n" +
                        "<p>* The seller is late and unresponsive for more than 24 hours while the order is marked as Late.</p>\n" +
                        "<p>* Users are abusive towards the other party through threats of low ratings or leveraging order materials (such as logins, personal information) against each other.</p>\n" +
                        "<p>* Users supplied or included copyright/trademark infringing materials as part of the Buyer requirements or the seller&rsquo;s delivery.</p>\n" +
                        "<p>* The user is no longer an active Shoclef user due to Terms of Use violations or closure of their account.</p>\n" +
                        "<p>* Delivered Orders (after the seller clicks Deliver Now and before the order is marked as complete)</p>\n" +
                        "<p>* The seller uses the Delivery system to extend the delivery due date to complete the requested service without providing the final delivered service to buyers.</p>\n" +
                        "<p>* Note: Multiple reported offenses will result in permanent suspension of your account.</p>\n" +
                        "<p>* The seller delivers no items/merchandise related to the agreed upon order requirements.</p>\n" +
                        "<p>* Note: Subjectivity of the materials in question will be reviewed by our Customer Support team.</p>\n" +
                        "<p>* The seller requests additional payments, on or off the Shoclef platform, by withholding the final delivery of services directly related to the agreed requirements.</p>\n" +
                        "<p>* The seller is withholding the final delivery of services for improved ratings.</p>\n" +
                        "<p>* Buyers who abuse the Request Revisions button to gain more services from sellers beyond the agreed requirements.</p>\n" +
                        "<p>* Buyers who threaten to leave a damaging review to gain more services from the seller not related to the agreed requirements.</p>\n" +
                        "<p>* Completed Orders (after the order is marked as complete and before the 14 day limitation)</p>\n" +
                        "<p>* Users who have been reported to use copyright/trademark infringing materials after verification and with proof.</p>\n" +
                        "<p>* Buyers who did not purchase commercial use rights and are reported to have used the materials commercially.</p>\n" +
                        "<p>* Note: Terms of Commercial use is found on the seller&rsquo;s Account page and cannot be retroactively included once the order is completed for over 14 days.</p>\n" +
                        "<p>* Shoclef Customer Support will review cases of Order delivery manipulation that prevents buyers and sellers from fully utilizing our Resolution Center that enabled the order to be marked as complete.</p>\n" +
                        "<p>Refunds</p>\n" +
                        "<p>* Shoclef does not automatically refund payments made for cancelled orders back to your payment provider. Funds from order cancellations are refunded to the buyer's balance as credit and are available for future purchases on Shoclef. Funds returned to your balance from cancelled orders will not include processing fees paid.</p>\n" +
                        "<p>* Deposit refunds, when available from the payment provider, can be performed by our Customer Support team. To prevent fraud and abuse, we limit the total amount of times users can request a payment provider refund from their account which is subject to review by our Customer Support team. Such refunds may be subject to an additional fee. If any processing fees were added at the time of purchase to create a new order, the processing fees from that payment will not be refunded along with your deposit.</p>\n" +
                        "<p>User Conduct and Protection</p>\n" +
                        "<p>Shoclef enables people around the world to create, share, sell and purchase nearly any service they need at an unbeatable value. Services offered on Shoclef.com. Users of the Shoclef community communicate and engage through orders, augemented reality, live players, live observers, live video, chat, phone calls, social media, and on Shoclef&rsquo;s Community Forums.</p>\n" +
                        "<p>Shoclef maintains a friendly, community spirited, and professional environment. Users should keep to that spirit while participating in any activity or extensions of Shoclef. This section relates to the expected conduct users should adhere to while interacting with each other on Shoclef.</p>\n" +
                        "<p>To report a violation of our Terms of Use, User Misconduct, or inquiries regarding your account, please contact our Customer Support team here.</p>\n" +
                        "<p>Basics</p>\n" +
                        "<p>* To protect our users' privacy, user identities are kept anonymous. Requesting or providing Email addresses, Skype/IM usernames, telephone numbers or any other personal contact details to communicate outside of Shoclef in order to circumvent or abuse the Shoclef messaging system or Shoclef platform is not permitted.</p>\n" +
                        "<p>* Any necessary exchange of personal information required to continue a service may be exchanged within the order page.</p>\n" +
                        "<p>* Shoclef does not provide any guarantee of the level of service offered to buyers.&nbsp;</p>\n" +
                        "<p>* Shoclef does not provide protection for users who interact outside of the Shoclef platform.</p>\n" +
                        "<p>* All information and file exchanges must be performed exclusively on Shoclef's platform.</p>\n" +
                        "<p>* Rude, abusive, improper language, or violent messages sent to users will not be tolerated and may result in an account warning or the suspension/removal of your account.</p>\n" +
                        "<p>* The Shoclef marketplace is open to everyone. Discrimination against a community member based on gender, race, age, religious affiliation, sexual preference or otherwise is not acceptable and may result in the suspension/removal of your account.</p>\n" +
                        "<p>Orders</p>\n" +
                        "<p>* Users with the intention to defame competing sellers by ordering from competing services will have their reviews removed or further account status related actions determined by review by our Trust &amp; Safety team.</p>\n" +
                        "<p>* Users are to refrain from spamming or soliciting previous Buyers or Sellers to pursue removing/modifying reviews or cancelling orders that do not align on Order Cancellation or Feedback policies.</p>\n" +
                        "<p>* Shoclef reviews cases of payment provider chargebacks and disputes on behalf of sellers. Although results vary per case due to each chargeback reason, we work hard on resolving disputes in the seller&rsquo;s favor. If the chargeback case allows, Shoclef will return parts or full revenue back to sellers.</p>\n" +
                        "<p>Reporting Violations</p>\n" +
                        "<p>If you come across any content that may violate our Terms of Use, you should report it to us through the appropriate channels created to handle those issues as outlined in our Terms of Use. All cases are reviewed by our Trust &amp; Safety team. To protect individual privacy, the results of the investigation are not shared. You can review our Privacy Policy for more information.</p>\n" +
                        "<p>Violations</p>\n" +
                        "<p>Users may receive a warning to their account for violations of our Terms of Use or any user misconduct reported to our Trust and Safety team. A warning will be sent to the user's email address and will be displayed for such user on the Site. Warnings do not limit account activity, but can lead to your account losing seller statuses or becoming permanently disabled based on the severity of the violation.</p>\n" +
                        "<p>Non-Permitted Usage</p>\n" +
                        "<p>Adult Services &amp; Pornography - Shoclef does not allow any exchange of adult oriented or pornographic materials and services.</p>\n" +
                        "<p>Inappropriate Behavior &amp; Language - Communication on Shoclef should be friendly, constructive, and professional. Shoclef condemns bullying, harassment, and hate speech towards others. We allow users a medium for which messages are exchanged between individuals, a system to rate orders, and to engage on larger platforms such as our Community Forum and Social Media pages.</p>\n" +
                        "<p>Phishing and Spam - Shoclef takes the matter its members&rsquo; security seriously. Any attempts to publish or send malicious content with the intent to compromise another member&rsquo;s account or computer environment is strictly prohibited. Please respect our members privacy by not contacting them for commercial purposes without their consent.</p>\n" +
                        "<p>Privacy &amp; Identity - You may not publish or post other people's private and confidential information. Any exchange of personal information required for the completion of a service must be provided in the order page. Sellers further confirm that whatever information they receive from the buyer, which is not public domain, shall not be used for any purpose whatsoever other than for the delivery of the work to the buyer. Any users who engage and communicate off of Shoclef will not be protected by our Terms of Use.</p>\n" +
                        "<p>Intellectual Property Claims - Shoclef will respond to clear and complete notices of alleged copyright or trademark infringement, and/or violation of third party&rsquo;s terms of service. Our Intellectual Property claims procedures can be reviewed here.</p>\n" +
                        "<p>Fraud / Unlawful Use - You may not use Shoclef for any unlawful purposes or to conduct illegal activities.</p>\n" +
                        "<p>Abuse and Spam</p>\n" +
                        "<p>Multiple Accounts - To prevent fraud and abuse, users are limited to one active account. Any additional account determined to be created to circumvent guidelines, promote competitive advantages, or mislead the Shoclef community will be disabled. Mass account creation may result in disabling of all related accounts. Note: any violations to Shoclef&rsquo;s Terms of Use is cause for permanent suspension of all accounts.</p>\n" +
                        "<p>Targeted Abuse - We do not tolerate users who engage in targeted abuse or harassment towards other users on Shoclef. This includes creating new multiple accounts to harass members through our message or ordering system.</p>\n" +
                        "<p>Selling Accounts - You may not buy or sell Shoclef accounts.</p>\n" +
                        "<p>General Terms</p>\n" +
                        "<p>* Shoclef reserves the right to put any account on hold or permanently disable accounts due to breach of these terms of service or due to any illegal or inappropriate use of the Site or services.</p>\n" +
                        "<p>* Violation of Shoclef's Terms of Use may get your account disabled permanently.</p>\n" +
                        "<p>* Users with disabled accounts will not be able to sell or buy on Shoclef.</p>\n" +
                        "<p>* Users who have violated our Terms of Use and had their account disabled may contact our Customer Support team for more information surrounding the violation and status of the account.</p>\n" +
                        "<p>* Users have the option to enable account Security features to protect their account from any unauthorized usage.</p>\n" +
                        "<p>* Users must be able to verify their account ownership through Customer Support by providing materials that prove ownership of that account.</p>\n" +
                        "<p>* Sellers will be able to withdraw their revenues from disabled accounts after a safety period of 90 days following full verification of ownership of the account in question, from the day of the last cleared payment received in their account and subject to Shoclef's approval.</p>\n" +
                        "<p>* Disputes should be handled using Shoclef's dispute resolution tools ('Disputes' on the order page) or by contacting Shoclef Customer Support.</p>\n" +
                        "<p>* Shoclef may make changes to its Terms of Service from time to time. When these changes are made, Shoclef will make a new copy of the terms of service available on this page.</p>\n" +
                        "<p>* You understand and agree that if you use Shoclef after the date on which the Terms of Service have changed, Shoclef will treat your use as acceptance of the updated Terms of Service.</p>\n" +
                        "<p>&nbsp;</p>"

        ));

    }

    private void setListeners() {
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
    }

    private void goBack() {
        SharedPref.saveBoolean(this.getClass().getSimpleName(), getApplicationContext(), SharedPref.tncActive, false);
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        finish();
    }
}