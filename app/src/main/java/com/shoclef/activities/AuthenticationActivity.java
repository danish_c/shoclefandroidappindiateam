package com.shoclef.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;
import com.shoclef.R;
import com.shoclef.utils.ApiClient;
import com.shoclef.utils.Fonts;
import com.shoclef.utils.LoggerClass;
import com.shoclef.utils.SharedPref;
import com.shoclef.utils.util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AuthenticationActivity extends AppCompatActivity {
    private Context context;
    private RequestQueue queue;
    private StringRequest stringRequest;
    private LinearLayout llChild, llLogin, llOtp, llResetPassword, llSignUp;
    private CountryCodePicker ccpLogin, ccpSignUp;
    private TextInputLayout tlUsernameLogin, tlPasswordLogin, tlOtp, tlPasswordResetPassword, tlRePasswordResetPassword, tlMobileNumberSignUp, tlUsernameSignUp, tlEmailSignUp, tlPasswordSignUp, tlRePasswordSignUp;
    private TextInputEditText tleUsernameLogin, tlePasswordLogin, tleOtp, tlePasswordResetPassword, tleRePasswordResetPassword, tleMobileNumberSignUp, tleUsernameSignUp, tleEmailSignUp, tlePasswordSignUp, tleRePasswordSignUp;
    private TextView tvPasswordHintLogin, tvForgotPasswordLogin, tvSignUpBtnLogin, tvLabelLoginOptions, tvLabelOtp, tvPasswordHintReset, tvSignUpUserPic, tvPasswordHintSignUp, tvTncSignUp;
    private Button btnLogin, btnVerifyOtp, btnResetPassword, btnSignUp;
    private Animation exitToLeft, enterFromRight, exitToRight, enterFromLeft;
    private ImageView ivFbLogin, ivGoogleLogin, ivSignUpAddPhoto, civSignUpUserPic;
    private CheckBox chbTncSignUp;
    private String
            mode = "Login",
            TAG = this.getClass().getSimpleName(),
            forgotPasswordMode = "Forgot Password",
            LoginMode = "Login",
            SignUpMode = "Sign Up",
            ResetPassword = "Reset Password",
            emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+",
            usernameError = "Please input a valid Mobile Number",
            passwordError = "Please input a valid Password",
            otpError = "Please enter a valid OTP",
            comparePasswordError = "Password does not match",
            nameError = "Please enter your Full Name",
            emailError = "Please enter a valid Email Address";
    private boolean doubleBackToExitPressedOnce = false;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        initialise();
    }

    private void initialise() {
        context = this.getApplicationContext();
        queue = Volley.newRequestQueue(context);
        progressDialog = new ProgressDialog(AuthenticationActivity.this);
        exitToLeft = AnimationUtils.loadAnimation(context, R.anim.exit_to_left);
        enterFromRight = AnimationUtils.loadAnimation(context, R.anim.enter_from_right);
        exitToRight = AnimationUtils.loadAnimation(context, R.anim.exit_to_right);
        enterFromLeft = AnimationUtils.loadAnimation(context, R.anim.enter_from_left);
        assignView();
    }

    private void assignView() {
        llChild = findViewById(R.id.ll_child);
        llLogin = findViewById(R.id.ll_login);
        llOtp = findViewById(R.id.ll_otp);
        llResetPassword = findViewById(R.id.ll_reset_password);
        llSignUp = findViewById(R.id.ll_sign_up);

        ccpLogin = findViewById(R.id.ccp_login);
        ccpSignUp = findViewById(R.id.ccp_sign_up);

        tlUsernameLogin = findViewById(R.id.tl_username_login);
        tlPasswordLogin = findViewById(R.id.tl_password_login);
        tlOtp = findViewById(R.id.tl_otp);
        tlPasswordResetPassword = findViewById(R.id.tl_password_reset_password);
        tlRePasswordResetPassword = findViewById(R.id.tl_re_password_reset_password);
        tlMobileNumberSignUp = findViewById(R.id.tl_mobile_number_sign_up);
        tlUsernameSignUp = findViewById(R.id.tl_username_sign_up);
        tlEmailSignUp = findViewById(R.id.tl_email_sign_up);
        tlPasswordSignUp = findViewById(R.id.tl_password_sign_up);
        tlRePasswordSignUp = findViewById(R.id.tl_re_password_sign_up);

        tleUsernameLogin = findViewById(R.id.tle_username_login);
        tlePasswordLogin = findViewById(R.id.tle_password_login);
        tleOtp = findViewById(R.id.tle_otp);
        tlePasswordResetPassword = findViewById(R.id.tle_password_reset_password);
        tleRePasswordResetPassword = findViewById(R.id.tle_re_password_reset_password);
        tleMobileNumberSignUp = findViewById(R.id.tle_mobile_number_sign_up);
        tleUsernameSignUp = findViewById(R.id.tle_username_sign_up);
        tleEmailSignUp = findViewById(R.id.tle_email_sign_up);
        tlePasswordSignUp = findViewById(R.id.tle_password_sign_up);
        tleRePasswordSignUp = findViewById(R.id.tle_re_password_sign_up);

        tvPasswordHintLogin = findViewById(R.id.tv_password_hint_login);
        tvForgotPasswordLogin = findViewById(R.id.tv_forgot_password_login);
        tvSignUpBtnLogin = findViewById(R.id.tv_sign_up_btn_login);
        tvLabelLoginOptions = findViewById(R.id.tv_label_login_options);
        tvLabelOtp = findViewById(R.id.tv_label_otp);
        tvPasswordHintReset = findViewById(R.id.tv_password_hint_reset);
        tvSignUpUserPic = findViewById(R.id.tv_sign_up_user_pic);
        tvPasswordHintSignUp = findViewById(R.id.tv_password_hint_sign_up);
        tvTncSignUp = findViewById(R.id.tv_tnc_sign_up);

        btnLogin = findViewById(R.id.btn_login);
        btnVerifyOtp = findViewById(R.id.btn_verify_otp);
        btnResetPassword = findViewById(R.id.btn_reset_password);
        btnSignUp = findViewById(R.id.btn_sign_up);

        ivFbLogin = findViewById(R.id.iv_fb_login);
        ivGoogleLogin = findViewById(R.id.iv_google_login);
        ivSignUpAddPhoto = findViewById(R.id.iv_sign_up_add_photo);
        civSignUpUserPic = findViewById(R.id.civ_sign_up_user_pic);

        chbTncSignUp = findViewById(R.id.chb_tnc_sign_up);

        setFonts();
        setListeners();
    }

    private void setListeners() {
        tvForgotPasswordLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode = forgotPasswordMode;
                gotoOtp();
            }
        });
        tvSignUpBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoSignUp();
            }
        });
        btnVerifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoResetPassword();
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode = SignUpMode;
                gotoOtp();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkIfDataEmpty(tleUsernameLogin, tlUsernameLogin, usernameError) && checkIfDataEmpty(tlePasswordLogin, tlPasswordLogin, passwordError)) {
                    hitLoginApi();
                }
            }
        });
        addTextChangeListeners(tleUsernameLogin, tlUsernameLogin);

        addTextChangeListeners(tlePasswordLogin, tlPasswordLogin);

        addTextChangeListeners(tleOtp, tlOtp);

        addTextChangeListeners(tlePasswordResetPassword, tlPasswordResetPassword);

        addTextChangeListeners(tleRePasswordResetPassword, tlRePasswordResetPassword);
        addTextChangeListeners(tleMobileNumberSignUp, tlMobileNumberSignUp);
        addTextChangeListeners(tleUsernameSignUp, tlUsernameSignUp);
        addTextChangeListeners(tleEmailSignUp, tlEmailSignUp);
        addTextChangeListeners(tlePasswordSignUp, tlPasswordSignUp);
        addTextChangeListeners(tleRePasswordSignUp, tlRePasswordSignUp);

        testingOn();
    }

    private void testingOn() {
        tleUsernameLogin.setText("7977909830");
        tlePasswordLogin.setText("Qwertyu@123");
    }

    private void hitLoginApi() {
        showProgressDialog("Checking Credentials.\nPlease wait...");
        final String methodName = "hitLoginApi";
        final String url = ApiClient.userLogin;
        stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dismissDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    LoggerClass.i(methodName, url);
                    LoggerClass.LogApiResponse(TAG, methodName, jsonObject);
                    String status = jsonObject.getString("status");
                    if (status.equals("Fail")) {
                        String message = jsonObject.getString("message");
                        if (message.equals("Phone Number does not exits")) {
                            tleUsernameLogin.requestFocus();
                            util.showKeyboard(context, tleUsernameLogin);
                            util.setError(tlUsernameLogin, tleUsernameLogin, message);
                        } else {
                            tlePasswordLogin.requestFocus();
                            util.showKeyboard(context, tlePasswordLogin);
                            util.setError(tlPasswordLogin, tlePasswordLogin, message);
                        }
                    } else {
                        String userData = jsonObject.getString("data");
                        JSONObject userDataJsonObj = new JSONObject(userData);
                        SaveData(SharedPref.accessToken, jsonObject.getString("token"));
                        SaveData(SharedPref.userId, userDataJsonObj.getString("user_id"));
                        SaveData(SharedPref.userName, userDataJsonObj.getString("fullname"));
                        SaveData(SharedPref.userNumber, userDataJsonObj.getString("phone_number"));
                        SaveData(SharedPref.userCountryCode, userDataJsonObj.getString("country_code"));
                        SaveData(SharedPref.userEmailId, userDataJsonObj.getString("email_id"));
                        SaveData(SharedPref.userCity, userDataJsonObj.getString("city"));
                        SaveData(SharedPref.userState, userDataJsonObj.getString("state"));
                        SaveData(SharedPref.userCcountry, userDataJsonObj.getString("country"));
                        gotoMain();
                    }

                    dismissDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoggerClass.e(methodName, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoggerClass.LogApiError(AuthenticationActivity.this, TAG, url, methodName, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("country_code", ccpLogin.getSelectedCountryCode());
                params.put("phone_number", Objects.requireNonNull(tleUsernameLogin.getText()).toString().trim());
                params.put("password", Objects.requireNonNull(tlePasswordLogin.getText()).toString().trim());
                LoggerClass.LogApi(TAG, methodName, url, String.valueOf(params));
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    }

    private void gotoMain() {
        util.gotoNextActivity(AuthenticationActivity.this, MainActivity.class);
        finish();
    }

    private void showProgressDialog(String s) {
        progressDialog.setMessage(s);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void dismissDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    private void addTextChangeListeners(final TextInputEditText textInputEditText,
                                        final TextInputLayout textInputLayout) {
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (textInputEditText.getHint().toString().contains("Password")) {
                    if (util.validatePassword(Objects.requireNonNull(textInputEditText.getText()).toString().trim())) {
                        util.removeError(textInputLayout);
                    }
                } else if (textInputEditText.getHint().toString().contains("OTP")) {
                    if (tleOtp.getText().toString().trim().length() == 6) {
                        util.removeError(textInputLayout);
                    }
                } else {
                    util.removeError(textInputLayout);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    private boolean checkIfDataEmpty(TextInputEditText textInputEditText, TextInputLayout
            textInputLayout, String errorMsg) {
        boolean isEmpty;
        if (errorMsg.equals(passwordError)) {
            if (!util.validatePassword(Objects.requireNonNull(textInputEditText.getText()).toString().trim())) {
                isEmpty = true;
                util.setError(textInputLayout, textInputEditText, errorMsg);
            } else {
                isEmpty = false;
            }
        } else if (errorMsg.equals(otpError)) {
            if (Objects.requireNonNull(textInputEditText.getText()).toString().trim().length() != 6) {
                isEmpty = true;
                util.setError(textInputLayout, textInputEditText, errorMsg);
            } else {
                isEmpty = false;
            }
        } else if (Objects.requireNonNull(textInputEditText.getText()).toString().trim().equals("")) {
            isEmpty = true;
            util.setError(textInputLayout, textInputEditText, errorMsg);
        } else if (errorMsg.equals(comparePasswordError)) {
            if (!Objects.requireNonNull(tlePasswordResetPassword.getText()).toString().trim().equals(textInputEditText.getText().toString().trim())) {
                isEmpty = true;
                util.setError(textInputLayout, textInputEditText, errorMsg);
            } else {
                isEmpty = false;
            }
        } else if (errorMsg.equals(nameError)) {
            if (textInputEditText.getText().toString().trim().equals("")) {
                isEmpty = true;
                util.setError(textInputLayout, textInputEditText, errorMsg);
            } else if (!textInputEditText.getText().toString().trim().contains(" ")) {
                isEmpty = true;
                util.setError(textInputLayout, textInputEditText, errorMsg);
            } else {
                isEmpty = false;
            }
        } else if (errorMsg.equals(emailError)) {
            if (!textInputEditText.getText().toString().trim().matches(emailPattern)) {
                isEmpty = true;
                util.setError(textInputLayout, textInputEditText, errorMsg);
            } else {
                isEmpty = false;
            }
        } else {
            isEmpty = false;
        }
        return !isEmpty;
    }


    private void gotoSignUp() {
        mode = SignUpMode;
        llChild.startAnimation(exitToLeft);
        llSignUp.startAnimation(enterFromRight);
        llChild.setVisibility(View.GONE);
        llLogin.setVisibility(View.GONE);
        llSignUp.setVisibility(View.VISIBLE);
    }

    private void gotoOtp() {
        if (mode.equals(ResetPassword)) {
            llResetPassword.startAnimation(exitToRight);
            llOtp.startAnimation(enterFromLeft);
            llResetPassword.setVisibility(View.GONE);
            llOtp.setVisibility(View.VISIBLE);
        } else {
            llLogin.startAnimation(exitToLeft);
            llOtp.startAnimation(enterFromRight);
            llLogin.setVisibility(View.GONE);
            llOtp.setVisibility(View.VISIBLE);
            llSignUp.setVisibility(View.GONE);
            llResetPassword.setVisibility(View.GONE);
        }
        mode = forgotPasswordMode;
    }

    private void gotoLogin() {
        if (mode.equals(SignUpMode)) {
            llChild.startAnimation(enterFromLeft);
            llSignUp.startAnimation(exitToRight);
            llChild.setVisibility(View.VISIBLE);
            llLogin.setVisibility(View.VISIBLE);
            llSignUp.setVisibility(View.GONE);
        } else {
            tleUsernameLogin.requestFocus();
            llOtp.startAnimation(exitToRight);
            llLogin.startAnimation(enterFromLeft);
            llLogin.setVisibility(View.VISIBLE);
            llOtp.setVisibility(View.GONE);
            llSignUp.setVisibility(View.GONE);
            llResetPassword.setVisibility(View.GONE);
        }
        mode = LoginMode;
    }

    private void gotoResetPassword() {
        mode = ResetPassword;
        tlePasswordResetPassword.requestFocus();
        llOtp.startAnimation(exitToLeft);
        llResetPassword.startAnimation(enterFromRight);
        llOtp.setVisibility(View.GONE);
        llResetPassword.setVisibility(View.VISIBLE);
    }


    @Override
    public void onBackPressed() {

        if (mode.equals(forgotPasswordMode)) {
            gotoLogin();
        } else if (mode.equals(SignUpMode)) {
            gotoLogin();
        } else if (mode.equals(ResetPassword)) {
            gotoOtp();
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            util.showToast("Please click BACK again to exit", context);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }

    private void setFonts() {
        Typeface light = Fonts.getLight(context);
        Typeface regular = Fonts.getRegular(context);
        Typeface bold = Fonts.getBold(context);
        ccpLogin.setTypeFace(regular);
        ccpSignUp.setTypeFace(regular);

        tlUsernameLogin.setTypeface(regular);
        tlPasswordLogin.setTypeface(regular);
        tlOtp.setTypeface(regular);
        tlPasswordResetPassword.setTypeface(regular);
        tlRePasswordResetPassword.setTypeface(regular);
        tlMobileNumberSignUp.setTypeface(regular);
        tlUsernameSignUp.setTypeface(regular);
        tlEmailSignUp.setTypeface(regular);
        tlPasswordSignUp.setTypeface(regular);
        tlRePasswordSignUp.setTypeface(regular);

        tleUsernameLogin.setTypeface(regular);
        tlePasswordLogin.setTypeface(regular);
        tleOtp.setTypeface(regular);
        tlePasswordResetPassword.setTypeface(regular);
        tleRePasswordResetPassword.setTypeface(regular);
        tleMobileNumberSignUp.setTypeface(regular);
        tleUsernameSignUp.setTypeface(regular);
        tleEmailSignUp.setTypeface(regular);
        tlePasswordSignUp.setTypeface(regular);
        tleRePasswordSignUp.setTypeface(regular);

        tvSignUpBtnLogin.setTypeface(light);
        tvSignUpUserPic.setTypeface(regular);
        tvTncSignUp.setTypeface(regular);

        tvForgotPasswordLogin.setTypeface(regular);

        tvLabelLoginOptions.setTypeface(light);
        tvLabelOtp.setTypeface(light);
        tvPasswordHintLogin.setTypeface(light);
        tvPasswordHintReset.setTypeface(regular);
        tvPasswordHintSignUp.setTypeface(regular);

        btnLogin.setTypeface(bold);
        btnVerifyOtp.setTypeface(bold);
        btnResetPassword.setTypeface(bold);
        btnSignUp.setTypeface(bold);


    }

    private void SaveData(String key, String value) {
        SharedPref.saveString(TAG, context, key, value);
    }

}
