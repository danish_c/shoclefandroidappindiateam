package com.shoclef.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.shoclef.R;
import com.shoclef.utils.Fonts;

public class ErrorScreenActivity extends AppCompatActivity {
    private Context context;
    private TextView tvLabel1, tvLabel2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_screen);
        initialise();
    }

    private void initialise() {
        context = this.getApplicationContext();
        assignViews();
    }

    private void assignViews() {
        tvLabel1 = findViewById(R.id.tv_label_1);
        tvLabel2 = findViewById(R.id.tv_label_2);
        setFonts();

    }

    private void setFonts() {
        tvLabel1.setTypeface(Fonts.getBold(context));
        tvLabel2.setTypeface(Fonts.getRegular(context));
    }
}
