package com.shoclef.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.shoclef.R;
import com.shoclef.utils.SharedPref;
import com.shoclef.utils.util;

public class SplashScreenActivity extends AppCompatActivity {

    Context context;
    private String TAG = this.getClass().getSimpleName();
    private RequestQueue queue;
    StringRequest stringRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        util.getFullScreen(SplashScreenActivity.this);
        setContentView(R.layout.activity_splash_screen);
        initialise();
    }

    private void initialise() {
        context = this.getApplicationContext();
        queue = Volley.newRequestQueue(context);
        gotoNextActivity();
    }

    private void gotoNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPref.getBoolean(TAG, context, SharedPref.autoLogin)) {
                    util.gotoNextActivity(SplashScreenActivity.this, MainActivity.class);
                } else {
                    util.gotoNextActivity(SplashScreenActivity.this, AuthenticationActivity.class);
                }
                finish();
            }
        }, 1000);
    }
}
