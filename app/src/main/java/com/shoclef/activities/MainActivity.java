package com.shoclef.activities;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.shoclef.R;
import com.shoclef.fragments.HotDealsDashboardFragment;
import com.shoclef.utils.SharedPref;
import com.shoclef.utils.util;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private Context context;
    private FragmentManager fragmentManager;
    private String TAG = this.getClass().getSimpleName();
    boolean doubleBackToExitPressedOnce = false;
    private TabLayout tabMain;
    private EditText etSearch;
    int streamers = 0, shopLive = 1, getLive = 2, hotDeals = 3, profile = 4;
    ImageView ivCart;
//Initial
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialise();
    }

    private void initialise() {
        context = this.getApplicationContext();
        fragmentManager = this.getSupportFragmentManager();
        SharedPref.saveBoolean(TAG, context, SharedPref.autoLogin, true);
        assignViews();
    }

    private void assignViews() {
        tabMain = findViewById(R.id.tab_main);
        etSearch = findViewById(R.id.et_search);
        ivCart = findViewById(R.id.iv_cart_header);
        ImageView iv_back_header = findViewById(R.id.iv_back_header);
        iv_back_header.setVisibility(View.GONE);
        setListeners();
        setupTabs();
    }

    private void setListeners() {

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                GoodsFragment.filter(etSearch.getText().toString().trim());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FragmentActivity.class);
                intent.putExtra("fragmentName", "Cart");
                intent.putExtra("backTo", "Main");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });
        ivCart.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent intent = new Intent(MainActivity.this, FragmentActivity.class);
                intent.putExtra("fragmentName", "Invoice");
                intent.putExtra("backTo", "Main");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                return false;
            }
        });
    }

    private void setupTabs() {
        setDefaultTab(hotDeals);
        bindWidgetsWithAnEvent();
    }

    private void bindWidgetsWithAnEvent() {
        tabMain.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Objects.requireNonNull(tabMain.getTabAt(tab.getPosition())).select();
                changeTabIcon(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                changeTabIcon(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                changeTabIcon(tab.getPosition());
            }
        });
    }

    private void setDefaultTab(int tabPosition) {
        changeTabIcon(tabPosition);
        Objects.requireNonNull(tabMain.getTabAt(tabPosition)).select();
    }

    private void changeTabIcon(int tabPosition) {
        if (tabPosition == streamers) {
//            setFragment(new StreamersDashboardFragment());
            showComingSoon();
            setTabImage(streamers, R.drawable.ic_streamers_selected);
            setTabImage(shopLive, R.drawable.ic_shop_live_unselected);
            setTabImage(getLive, R.drawable.ic_get_live_unselected);
            setTabImage(hotDeals, R.drawable.ic_hot_deals_unselected);
            setTabImage(profile, R.drawable.ic_profile_unselected);
        } else if (tabPosition == shopLive) {
//            setFragment(new ShopLiveDashboardFragment());
            showComingSoon();
            setTabImage(streamers, R.drawable.ic_streamers_unselected);
            setTabImage(shopLive, R.drawable.ic_shop_live_selected);
            setTabImage(getLive, R.drawable.ic_get_live_unselected);
            setTabImage(hotDeals, R.drawable.ic_hot_deals_unselected);
            setTabImage(profile, R.drawable.ic_profile_unselected);
        } else if (tabPosition == getLive) {
//            setFragment(new GetLiveDashboardFragment());
            showComingSoon();
            setTabImage(streamers, R.drawable.ic_streamers_unselected);
            setTabImage(shopLive, R.drawable.ic_shop_live_unselected);
            setTabImage(getLive, R.drawable.ic_get_live_selected);
            setTabImage(hotDeals, R.drawable.ic_hot_deals_unselected);
            setTabImage(profile, R.drawable.ic_profile_unselected);
        } else if (tabPosition == hotDeals) {
            setFragment(new HotDealsDashboardFragment());
            setTabImage(streamers, R.drawable.ic_streamers_unselected);
            setTabImage(shopLive, R.drawable.ic_shop_live_unselected);
            setTabImage(getLive, R.drawable.ic_get_live_unselected);
            setTabImage(hotDeals, R.drawable.ic_hot_deals_selected);
            setTabImage(profile, R.drawable.ic_profile_unselected);
        } else if (tabPosition == profile) {
//            setFragment(new ProfileDashboardFragment());
            showComingSoon();
            setTabImage(streamers, R.drawable.ic_streamers_unselected);
            setTabImage(shopLive, R.drawable.ic_shop_live_unselected);
            setTabImage(getLive, R.drawable.ic_get_live_unselected);
            setTabImage(hotDeals, R.drawable.ic_hot_deals_unselected);
            setTabImage(profile, R.drawable.ic_profile_selected);
        }
    }

    private void setTabImage(int tab, @DrawableRes int resId) {
        Objects.requireNonNull(tabMain.getTabAt(tab)).setIcon(resId);
    }

    private void setFragment(Fragment fragment) {
        util.openFragment(R.id.fl_main_parent, fragment, fragmentManager);
    }

    private void showComingSoon() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("We are working on it!!!!!");
        AlertDialog alert = builder.create();
        alert.setTitle("Coming Soon");
        alert.show();
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                setupTabs();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                setupTabs();
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        util.showToast("Please click BACK again to exit", context);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}