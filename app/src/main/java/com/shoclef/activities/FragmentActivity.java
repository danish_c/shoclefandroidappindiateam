package com.shoclef.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.shoclef.R;
import com.shoclef.fragments.GoodsProductDetailsFragment;
import com.shoclef.fragments.InvoiceFragment;

public class FragmentActivity extends AppCompatActivity {
    private FragmentManager fragmentManager;
    private String backTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        initialise();
    }

    private void initialise() {
        fragmentManager = this.getSupportFragmentManager();
        try {
            backTo = getIntent().getStringExtra("backTo");
        } catch (NullPointerException e) {
            backTo = "Mainactivity";
        } catch (Exception e) {
            backTo = "Mainactivity";
            e.printStackTrace();
        }
        receiveArguments();
    }

    private void receiveArguments() {
        String fragmentName = getIntent().getStringExtra("fragmentName");

        if (fragmentName != null) {
            switch (fragmentName) {
                case "Product Detail":
                    Fragment GoodsDetailsFragment = new GoodsProductDetailsFragment();
                    Bundle GoodsDetailsBundle = new Bundle();
                    GoodsDetailsBundle.putString("productId", getIntent().getStringExtra("productId"));
                    GoodsDetailsBundle.putString("jsonObject", getIntent().getStringExtra("jsonObject"));
                    if (backTo.equals("Cart")) {
                        GoodsDetailsBundle.putString("quantity", getIntent().getStringExtra("quantity"));
                    }
                    GoodsDetailsBundle.putString("backTo", getIntent().getStringExtra("backTo"));
                    GoodsDetailsFragment.setArguments(GoodsDetailsBundle);
                    FragmentTransaction GoodsDetailsFragmentTransaction = fragmentManager.beginTransaction();
                    GoodsDetailsFragmentTransaction.replace(R.id.fl_fragment_layout, GoodsDetailsFragment)
                            .setCustomAnimations(0, 0)
                            .addToBackStack(String.valueOf(GoodsDetailsFragment))
                            .commit();
                    break;
                case "Invoice":
                    Fragment InvoiceFragment = new InvoiceFragment();
                    FragmentTransaction InvoiceFragmentTransaction = fragmentManager.beginTransaction();
                    Bundle InvoiceDetailBundle = new Bundle();
                    InvoiceDetailBundle.putString("backTo", backTo);
                    InvoiceFragment.setArguments(InvoiceDetailBundle);
                    InvoiceFragmentTransaction.replace(R.id.fl_fragment_layout, InvoiceFragment)
                            .setCustomAnimations(0, 0)
                            .addToBackStack(String.valueOf(InvoiceFragment))
                            .commit();
                    break;
            }
        }


    }
}
